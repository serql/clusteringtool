package controller.clustering;

import model.TriFunction;
import model.api.Cluster;
import model.api.MDG;
import model.api.Node;
import model.impl.ClusterImpl;
import model.impl.MDGImpl;
import model.impl.NodeImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

/**
 * @author Georgia Grigoriadou
 */

@SuppressWarnings("unchecked")
public class NeighbourPartitionGeneratorTest {
    private MDG mdg         = new MDGImpl();
    private Cluster cluster = new ClusterImpl();

    private Node nodeA;
    private Node nodeB;
    private Node nodeC;
    private Node nodeD;

    @Before
    public void setup() {
        Cluster clusterA = new ClusterImpl();
        Cluster clusterB = new ClusterImpl();
        nodeA       = new NodeImpl(1, "A");
        nodeB       = new NodeImpl(2, "B");
        nodeC       = new NodeImpl(3, "C");
        nodeD       = new NodeImpl(4, "D");

        nodeA.addNeighbour(nodeB, (float) 1.0);
        nodeA.addNeighbour(nodeC, (float) 1.0);
        nodeB.addNeighbour(nodeA, (float) 1.0);
        nodeB.addNeighbour(nodeC, (float) 1.0);
        nodeC.addNeighbour(nodeA, (float) 1.0);
        nodeC.addNeighbour(nodeB, (float) 1.0);
        nodeC.addNeighbour(nodeD, (float) 1.0);
        nodeD.addNeighbour(nodeC, (float) 1.0);

        mdg.addCluster(clusterA);
        mdg.addCluster(clusterB);

        mdg.addNodeToTargetCluster(clusterA, nodeA);
        mdg.addNodeToTargetCluster(clusterA, nodeD);
        mdg.addNodeToTargetCluster(clusterB, nodeB);
        mdg.addNodeToTargetCluster(clusterB, nodeC);

        cluster = clusterA;
    }

    @Test
    public void testGenerate() throws Exception {
        final NeighbourPartitionGenerator npg = new NeighbourPartitionGenerator();

        MDG result = npg.generate.apply(mdg);

        Assert.assertEquals(result.getModularizationQuality(), 1, 0.0);
    }

    @Test
    public void testCreateNeighbourPartition() throws Exception {
        final NeighbourPartitionGenerator npg = new NeighbourPartitionGenerator();

        final Field field = npg.getClass().getDeclaredField("createNeighbourPartition");
        field.setAccessible(true);
        TriFunction<MDG,Cluster,Node, MDG> function = (TriFunction<MDG, Cluster, Node, MDG>) field.get(npg);

        MDG result = function.apply(mdg, cluster, nodeA);

        Assert.assertEquals(result.getModularizationQuality(), 1, 0.0);
    }

    @Test
    public void tesCreateNPByMovingInExistingCluster() throws Exception {
        final NeighbourPartitionGenerator npg = new NeighbourPartitionGenerator();

        final Field field = npg.getClass().getDeclaredField("createNPByMovingInExistingCluster");
        field.setAccessible(true);
        TriFunction<MDG,Cluster,Node, MDG> function = (TriFunction<MDG, Cluster, Node, MDG>) field.get(npg);

        MDG result = function.apply(mdg, cluster, nodeA);

        // Assert that clusterA{nodeA, nodeB,nodeC, nodeD}
        Assert.assertEquals(result.getClusters().size(), 2);
        result.getClusters().stream().filter(cluster -> cluster.getNodes().contains(nodeA)).forEach(cluster -> {
            Assert.assertTrue(cluster.getNodes().contains(nodeA));
            Assert.assertTrue(cluster.getNodes().contains(nodeB));
            Assert.assertTrue(cluster.getNodes().contains(nodeC));
            Assert.assertTrue(cluster.getNodes().contains(nodeD));
        });

    }

    @Test
    public void testCreateNPByAddingANewCluster() throws Exception {
        final NeighbourPartitionGenerator npg = new NeighbourPartitionGenerator();

        final Field field = npg.getClass().getDeclaredField("createNPByAddingANewCluster");
        field.setAccessible(true);
        TriFunction<MDG, Cluster, Node, MDG> function = (TriFunction<MDG, Cluster, Node, MDG>) field.get(npg);

        MDG result = function.apply(mdg, cluster, nodeA);

        // Assert that clusterA{nodeA}, clusterB{nodeB,nodeC,, nodeD}
        Assert.assertEquals(result.getClusters().size(), 3);
        for (Cluster cluster : result.getClusters()) {
            if (cluster.getNodes().contains(nodeB)) {
                Assert.assertTrue(cluster.getNodes().contains(nodeD));
                Assert.assertTrue(cluster.getNodes().contains(nodeB));
                Assert.assertTrue(cluster.getNodes().contains(nodeC));

            } else if(cluster.getNodes().size() == 1){
                Assert.assertTrue(cluster.getNodes().contains(nodeA));
            }
        }
    }

    @Test
    public void testCreateNPByRemovingABlock() throws Exception {
        final NeighbourPartitionGenerator npg = new NeighbourPartitionGenerator();

        final Field field = npg.getClass().getDeclaredField("createNPByRemovingABlock");
        field.setAccessible(true);
        TriFunction<MDG,Cluster,Node, MDG> function = (TriFunction<MDG, Cluster, Node, MDG>) field.get(npg);

        MDG result = function.apply(mdg, cluster, nodeA);

        // Assert that clusterA{nodeA, nodeB}, clusterB{nodeC}, clusterC{nodeD},
        Assert.assertEquals(result.getClusters().size(), 3);
        for (Cluster cluster : result.getClusters()) {
            if (cluster.getNodes().contains(nodeA)) {
                Assert.assertTrue(cluster.getNodes().contains(nodeA));
                Assert.assertTrue(cluster.getNodes().contains(nodeB));

            } else if (cluster.getNodes().contains(nodeC)) {
                Assert.assertTrue(cluster.getNodes().contains(nodeC));
                Assert.assertTrue(cluster.getNodes().contains(nodeD));
            }
        }
    }

}
