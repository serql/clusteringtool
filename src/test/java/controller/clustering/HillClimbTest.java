package controller.clustering;

import model.api.Graph;
import model.api.MDG;
import model.impl.ClusterImpl;
import model.impl.MDGImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import controller.GraphInitializer;

/**
 * @author Georgia Grigoriadou
 */

public class HillClimbTest {
    private Graph graph1;
    private Graph graph2;
    private Graph graph3;
    private Graph graph4;
    private Graph graph5;
    private Graph graph6;

    @Before
    public void setup(){
        GraphInitializer graphInit = new GraphInitializer();
        graph1 = graphInit.initialize.apply("src/main/resources/tests/test1/nodes.csv","src/main/resources/tests/test1/edges.csv", true);
        graph2 = graphInit.initialize.apply("src/main/resources/tests/test2/nodes.csv","src/main/resources/tests/test2/edges.csv", true);
        graph3 = graphInit.initialize.apply("src/main/resources/tests/test3/nodes.csv","src/main/resources/tests/test3/edges.csv", true);
        graph4 = graphInit.initialize.apply("src/main/resources/tests/test4/nodes.csv","src/main/resources/tests/test4/edges.csv", true);
        graph5 = graphInit.initialize.apply("src/main/resources/tests/test5/nodes.csv","src/main/resources/tests/test5/edges.csv", true);
        graph6 = graphInit.initialize.apply("src/main/resources/tests/test6/nodes.csv","src/main/resources/tests/test6/edges.csv", true);
    }

    @Test
    public void testCluster1() throws Exception {
        final HillClimb hillClimb = new HillClimb();

        MDG mdg = hillClimb.cluster(graph1, new MDGImpl(), new ClusterImpl(), 10);

        Assert.assertEquals(mdg.getModularizationQuality(), (float) 1.9285717, 0.001);
    }

    @Test
    public void testCluster2() throws Exception {
        final HillClimb hillClimb = new HillClimb();

        MDG mdg = hillClimb.cluster(graph2, new MDGImpl(), new ClusterImpl(), 10);

        Assert.assertEquals(mdg.getModularizationQuality(), (float) 1.5064936, 0.001);
    }

    @Test
    public void testCluster3() throws Exception {
        final HillClimb hillClimb = new HillClimb();

        MDG mdg = hillClimb.cluster(graph3, new MDGImpl(), new ClusterImpl(), 10);

        Assert.assertEquals(mdg.getModularizationQuality(), (float) 1.9238095, 0.001);
    }

    @Test
    public void testCluster4() throws Exception {
        final HillClimb hillClimb = new HillClimb();

        MDG mdg = hillClimb.cluster(graph4, new MDGImpl(), new ClusterImpl(), 10);

        Assert.assertEquals(mdg.getModularizationQuality(), (float) 1.6281676, 0.001);
    }

    @Test
    public void testCluster5() throws Exception {
        final HillClimb hillClimb = new HillClimb();

        MDG mdg = hillClimb.cluster(graph5, new MDGImpl(), new ClusterImpl(), 10);

        Assert.assertEquals(mdg.getModularizationQuality(), (float) 1.9615386, 0.001);
    }

    @Test
    public void testCluster6() throws Exception {
        final HillClimb hillClimb = new HillClimb();

        MDG mdg = hillClimb.cluster(graph6, new MDGImpl(), new ClusterImpl(), 10);

        Assert.assertEquals(mdg.getModularizationQuality(), (float) 2.3000002, 0.001);
    }

}
