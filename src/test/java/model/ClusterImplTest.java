package model;


import model.api.Cluster;
import model.api.Node;
import model.impl.ClusterImpl;
import model.impl.NodeImpl;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Georgia Grigoriadou
 */

@SuppressWarnings("unchecked")
public class ClusterImplTest {

    @Test
    public void testGetId() throws Exception {
        final ClusterImpl cluster = new ClusterImpl();
        final Field field         = cluster.getClass().getDeclaredField("id");
        field.setAccessible(true);
        String id = "first_cluster";
        field.set(cluster, id);

        String result = cluster.getId();

        Assert.assertEquals(id, result);
    }

    @Test
    public void testSetId() throws Exception {
        final ClusterImpl cluster = new ClusterImpl();
        String id                 = "first_cluster";
        cluster.setId(id);

        final Field field = cluster.getClass().getDeclaredField("id");
        field.setAccessible(true);

        Assert.assertEquals(field.get(cluster), id);
    }

    @Test
    public void testGetNodes() throws Exception {
        final ClusterImpl cluster = new ClusterImpl();
        final Field field         = cluster.getClass().getDeclaredField("nodes");
        field.setAccessible(true);
        Set<Node> nodes = new HashSet<>();
        Node node        = new NodeImpl(0, "testNode");
        nodes.add(node);
        field.set(cluster, nodes);

        Set<Node> result = cluster.getNodes();

        Assert.assertEquals(nodes, result);
        Assert.assertTrue(result.contains(node));
    }

    @Test
    public void testSetNodes() throws Exception {
        final ClusterImpl cluster = new ClusterImpl();
        Set<Node> nodes           = new HashSet<>();
        Node node = new NodeImpl(0, "testNode");
        nodes.add(node);
        cluster.setNodes(nodes);

        final Field field = cluster.getClass().getDeclaredField("nodes");
        field.setAccessible(true);
        Set<Node> result = (Set<Node>) field.get(cluster);

        Assert.assertEquals(field.get(cluster), nodes);
        Assert.assertTrue(result.contains(node));


    }

    @Test
    public void testSetInteredges() throws Exception {
        final ClusterImpl cluster = new ClusterImpl();
        Float interEdges          = (float) 10.0;
        cluster.setInterEdges(interEdges);

        final Field field = cluster.getClass().getDeclaredField("interEdges");
        field.setAccessible(true);

        Assert.assertEquals(field.get(cluster), interEdges);
    }

    @Test
    public void testSetIntraedges() throws Exception {
        final ClusterImpl cluster = new ClusterImpl();
        Float intraEdges          = (float) 10.0;
        cluster.setIntraEdges(intraEdges);

        final Field field = cluster.getClass().getDeclaredField("intraEdges");
        field.setAccessible(true);

        Assert.assertEquals(field.get(cluster), intraEdges);
    }

    @Test
    public void testGetClusterImplFactor() throws Exception {
        final ClusterImpl cluster = new ClusterImpl();
        final Field field         = cluster.getClass().getDeclaredField("clusterFactor");
        field.setAccessible(true);
        Float clusterFactor = (float) 10.0;
        field.set(cluster, clusterFactor);

        Float result = cluster.getClusterFactor();

        Assert.assertEquals(clusterFactor, result);
    }

    @Test
    public void testSetClusterImplFactor() throws Exception {
        final ClusterImpl cluster = new ClusterImpl();
        Float clusterFactor       = (float) 10.0;
        cluster.setClusterFactor(clusterFactor);

        final Field field = cluster.getClass().getDeclaredField("clusterFactor");
        field.setAccessible(true);

        Assert.assertEquals(field.get(cluster), clusterFactor);
    }

    @Test
    public void testCalculateClusterImplFactor() throws Exception{
        final ClusterImpl cluster = new ClusterImpl();
        final Field cfField       = cluster.getClass().getDeclaredField("clusterFactor");
        final Field intraField    = cluster.getClass().getDeclaredField("intraEdges");
        final Field interField    = cluster.getClass().getDeclaredField("interEdges");
        cfField.setAccessible(true);
        interField.setAccessible(true);
        intraField.setAccessible(true);

        Float intraEdges = (float) 10.0;
        Float interEdges = (float) 5.0;
        intraField.set(cluster, intraEdges);
        interField.set(cluster, interEdges);

        cluster.calculateClusterFactor();

        Assert.assertEquals(cfField.get(cluster), (float) 0.8);
    }

    @Test
    public void testCalculateClusterImplFactorIsZero() throws Exception{
        final ClusterImpl cluster = new ClusterImpl();
        final Field cfField       = cluster.getClass().getDeclaredField("clusterFactor");
        final Field intraField    = cluster.getClass().getDeclaredField("intraEdges");
        cfField.setAccessible(true);
        intraField.setAccessible(true);

        Float intraEdges = (float) 0.0;
        intraField.set(cluster, intraEdges);

        cluster.calculateClusterFactor();

        Assert.assertEquals(cfField.get(cluster), (float) 0.0);
    }

    @Test
    public void testAddNodeInEmptyList() throws Exception{
        final ClusterImpl cluster = new ClusterImpl();
        Node node                 = new NodeImpl(0, "testNode");

        for(int i = 1; i < 5; i++) {
            Node neighbourNode = new NodeImpl(i, "testNode");
            node.addNeighbour(neighbourNode, (float) 1.0);
        }

        cluster.addNode(node);

        final Field nodesField = cluster.getClass().getDeclaredField("nodes");
        final Field cfField    = cluster.getClass().getDeclaredField("clusterFactor");
        final Field intraField = cluster.getClass().getDeclaredField("intraEdges");
        final Field interField = cluster.getClass().getDeclaredField("interEdges");

        nodesField.setAccessible(true);
        cfField.setAccessible(true);
        intraField.setAccessible(true);
        interField.setAccessible(true);

        Set<Node> result = (Set<Node>) nodesField.get(cluster);

        Assert.assertTrue(result.contains(node));
        Assert.assertEquals(intraField.get(cluster), (float) 0.0);
        Assert.assertEquals(interField.get(cluster), (float) 4.0);
        Assert.assertEquals(cfField.get(cluster), (float) 0.0);
    }

    @Test
    public void testAddNode() throws Exception{
        final ClusterImpl cluster = new ClusterImpl();

        Node node  = new NodeImpl(0, "testNode");
        Node node1 = new NodeImpl(1, "testNode");
        Node node2 = new NodeImpl(2, "testNode");

        node.addNeighbour(node1, (float) 1.0);
        node.addNeighbour(node2, (float) 1.0);
        node1.addNeighbour(node, (float) 1.0);
        node1.addNeighbour(node2,(float) 1.0);
        node2.addNeighbour(node, (float) 1.0);
        node2.addNeighbour(node1,(float) 1.0);

        Set<Node> nodes = new HashSet<>();
        nodes.add(node1);

        Field nodesField = cluster.getClass().getDeclaredField("nodes");
        Field cfField    = cluster.getClass().getDeclaredField("clusterFactor");
        Field intraField = cluster.getClass().getDeclaredField("intraEdges");
        Field interField = cluster.getClass().getDeclaredField("interEdges");

        nodesField.setAccessible(true);
        cfField.setAccessible(true);
        intraField.setAccessible(true);
        interField.setAccessible(true);

        nodesField.set(cluster, nodes);
        intraField.set(cluster, (float) 0.0);
        interField.set(cluster, (float) 2.0);
        cfField.set(cluster,    (float) 0.0);

        cluster.addNode(node);

        Set<Node> result = (Set<Node>) nodesField.get(cluster);

        Assert.assertTrue(result.contains(node));
        Assert.assertEquals(intraField.get(cluster), (float) 1.0);
        Assert.assertEquals(interField.get(cluster), (float) 2.0);
        Assert.assertEquals(cfField.get(cluster),    (float) 0.5);
    }

    @Test
    public void testAddNodeThatAlreadyExist() throws Exception{
        final ClusterImpl cluster = new ClusterImpl();
        Node node             = new NodeImpl(0, "testNode");

        Set<Node> nodes = new HashSet<>();
        nodes.add(node);

        Field nodesField = cluster.getClass().getDeclaredField("nodes");
        Field cfField    = cluster.getClass().getDeclaredField("clusterFactor");
        Field intraField = cluster.getClass().getDeclaredField("intraEdges");
        Field interField = cluster.getClass().getDeclaredField("interEdges");

        nodesField.setAccessible(true);
        cfField.setAccessible(true);
        intraField.setAccessible(true);
        interField.setAccessible(true);

        nodesField.set(cluster, nodes);
        intraField.set(cluster, (float) 0.0);
        interField.set(cluster, (float) 0.0);
        cfField.set(cluster,    (float) 0.0);

        cluster.addNode(node);

        Set<Node> result = (Set<Node>) nodesField.get(cluster);

        Assert.assertEquals(result.size(), 1);
        Assert.assertEquals(intraField.get(cluster), (float) 0.0);
        Assert.assertEquals(interField.get(cluster), (float) 0.0);
        Assert.assertEquals(cfField.get(cluster),    (float) 0.0);
    }

    @Test
    public void testRemoveNode() throws Exception{
        final ClusterImpl cluster = new ClusterImpl();

        Node node  = new NodeImpl(0, "testNode");
        Node node1 = new NodeImpl(1, "testNode");
        Node node2 = new NodeImpl(2, "testNode");

        node.addNeighbour(node1, (float) 1.0);
        node.addNeighbour(node2, (float) 1.0);
        node1.addNeighbour(node, (float) 1.0);
        node1.addNeighbour(node2,(float) 1.0);
        node2.addNeighbour(node, (float) 1.0);
        node2.addNeighbour(node1,(float) 1.0);

        Set<Node> nodes = new HashSet<>();
        nodes.add(node);
        nodes.add(node1);

        Field nodesField = cluster.getClass().getDeclaredField("nodes");
        Field cfField    = cluster.getClass().getDeclaredField("clusterFactor");
        Field intraField = cluster.getClass().getDeclaredField("intraEdges");
        Field interField = cluster.getClass().getDeclaredField("interEdges");

        nodesField.setAccessible(true);
        cfField.setAccessible(true);
        interField.setAccessible(true);
        intraField.setAccessible(true);

        nodesField.set(cluster, nodes);
        intraField.set(cluster, (float) 1.0);
        interField.set(cluster, (float) 2.0);
        cfField.set(cluster,    (float) 0.5);

        cluster.removeNode(node);

        Set<Node> result = (Set<Node>) nodesField.get(cluster);

        Assert.assertFalse(result.contains(node));
        Assert.assertEquals(intraField.get(cluster), (float) 0.0);
        Assert.assertEquals(interField.get(cluster), (float) 2.0);
        Assert.assertEquals(cfField.get(cluster),    (float) 0.0);
    }

    @Test
    public void testRemoveNodeThatDoesNotExist() throws Exception{
        final ClusterImpl cluster = new ClusterImpl();

        Node node  = new NodeImpl(0, "testNode");
        Node node1 = new NodeImpl(1, "testNode");

        Set<Node> nodes = new HashSet<>();
        nodes.add(node1);

        Field nodesField = cluster.getClass().getDeclaredField("nodes");
        Field cfField    = cluster.getClass().getDeclaredField("clusterFactor");
        Field intraField = cluster.getClass().getDeclaredField("intraEdges");
        Field interField = cluster.getClass().getDeclaredField("interEdges");

        nodesField.setAccessible(true);
        cfField.setAccessible(true);
        interField.setAccessible(true);
        intraField.setAccessible(true);

        nodesField.set(cluster, nodes);
        cluster.removeNode(node);

        Set<Node> result = (Set<Node>) nodesField.get(cluster);

        Assert.assertEquals(result.size(), 1);
        Assert.assertEquals(intraField.get(cluster), (float) 0.0);
        Assert.assertEquals(interField.get(cluster), (float) 0.0);
        Assert.assertEquals(cfField.get(cluster),    (float) 0.0);
    }

    @Test
    public void testClone() throws Exception{
        final ClusterImpl cluster = new ClusterImpl();

        Node node  = new NodeImpl(0, "testNodes");
        Node node1 = new NodeImpl(1, "testNodes");
        Node node2 = new NodeImpl(2, "testNodes");

        node.addNeighbour(node1, (float) 1.0);
        node.addNeighbour(node2, (float) 1.0);
        node1.addNeighbour(node, (float) 1.0);
        node1.addNeighbour(node2,(float) 1.0);
        node2.addNeighbour(node, (float) 1.0);
        node2.addNeighbour(node1,(float) 1.0);

        Set<Node> nodes = new HashSet<>();
        nodes.add(node);
        nodes.add(node1);
        nodes.add(node2);

        Field nodesField = cluster.getClass().getDeclaredField("nodes");
        Field cfField    = cluster.getClass().getDeclaredField("clusterFactor");
        Field intraField = cluster.getClass().getDeclaredField("intraEdges");
        Field interField = cluster.getClass().getDeclaredField("interEdges");

        nodesField.setAccessible(true);
        cfField.setAccessible(true);
        intraField.setAccessible(true);
        interField.setAccessible(true);

        nodesField.set(cluster, nodes);
        intraField.set(cluster, (float) 3.0);
        interField.set(cluster, (float) 0.0);
        cfField.set(cluster,    (float) 1.0);

        Cluster newCluster = cluster.clone();

        Field newNodesField = newCluster.getClass().getDeclaredField("nodes");
        Field newCfField    = newCluster.getClass().getDeclaredField("clusterFactor");
        Field newIntraField = newCluster.getClass().getDeclaredField("intraEdges");
        Field newInterField = newCluster.getClass().getDeclaredField("interEdges");

        newNodesField.setAccessible(true);
        newCfField.setAccessible(true);
        newIntraField.setAccessible(true);
        newInterField.setAccessible(true);

        Set<Node> newNodes = (Set<Node>) newNodesField.get(cluster);

        Assert.assertTrue(newNodes.contains(node));
        Assert.assertTrue(newNodes.contains(node1));
        Assert.assertTrue(newNodes.contains(node2));
        Assert.assertEquals(newIntraField.get(cluster), (float) 3.0);
        Assert.assertEquals(newInterField.get(cluster), (float) 0.0);
        Assert.assertEquals(newCfField.get(cluster),    (float) 1.0);
    }

}
