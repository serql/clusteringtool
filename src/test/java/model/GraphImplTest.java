package model;

import model.api.Edge;
import model.api.Node;
import model.impl.EdgeImpl;
import model.impl.GraphImpl;
import model.impl.NodeImpl;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Georgia Grigoriadou
 */

@SuppressWarnings("unchecked")
public class GraphImplTest {

    @Test
    public void testGetNodes() throws Exception{
        final GraphImpl graph = new GraphImpl();
        final Field field     = graph.getClass().getDeclaredField("nodes");
        field.setAccessible(true);
        Set<Node> nodes = new HashSet<>();
        Node node       = new NodeImpl(0, "testNode");
        nodes.add(node);
        field.set(graph, nodes);

        Set<Node> result = graph.getNodes();

        Assert.assertEquals(nodes, result);
        Assert.assertTrue(result.contains(node));
    }

    @Test
    public void testSetNodes() throws Exception{
        final GraphImpl graph = new GraphImpl();
        Set<Node> nodes = new HashSet<>();
        Node node       = new NodeImpl(0, "testNode");
        nodes.add(node);
        graph.setNodes(nodes);

        final Field field = graph.getClass().getDeclaredField("nodes");
        field.setAccessible(true);

        Set<Node> newNodes = (Set<Node>) field.get(graph);

        Assert.assertTrue(newNodes.contains(node));
        Assert.assertEquals(field.get(graph), nodes);
    }

    @Test
    public void testGetEdges() throws Exception{
        final GraphImpl graph = new GraphImpl();
        final Field field = graph.getClass().getDeclaredField("edges");
        field.setAccessible(true);
        Set<Edge> edges = new HashSet<>();
        Edge edge       = new EdgeImpl(0, 1, "Directed", (float) 1.0);
        edges.add(edge);
        field.set(graph, edges);

        Set<Edge> result = graph.getEdges();

        Assert.assertEquals(edges, result);
        Assert.assertTrue(result.contains(edge));
    }

    @SuppressWarnings("all")
    @Test
    public void testSetEdges() throws Exception{
        final GraphImpl graph = new GraphImpl();
        Set<Edge> edges       = new HashSet<>();
        Edge edge = new EdgeImpl(0, 1, "Directed", (float) 1.0);
        edges.add(edge);
        graph.setEdges(edges);

        final Field field = graph.getClass().getDeclaredField("edges");
        field.setAccessible(true);

        Set<EdgeImpl> newNodes = (Set<EdgeImpl>) field.get(graph);

        Assert.assertTrue(newNodes.contains(edge));
        Assert.assertEquals(field.get(graph), edges);
    }
}
