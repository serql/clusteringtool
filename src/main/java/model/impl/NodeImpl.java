package model.impl;

import model.api.Node;

import java.util.HashMap;

/**
 * @author Georgia Grigoriadou
 */

public class NodeImpl implements Node {
    private int                  id;
    private String               name;
    private HashMap<Node, Float> neighbours;

    public NodeImpl() { this.neighbours = new HashMap<>(); }

    public NodeImpl(int id, String name) {
        this.id         = id;
        this.name       = name;
        this.neighbours = new HashMap<>();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public HashMap<Node, Float> getNeighbours() {
        return neighbours;
    }

    @Override
    public void addNeighbour(Node neighbour, float weight) {
        this.neighbours.put(neighbour, weight);
    }
}
