package model.impl;

import model.api.Cluster;
import model.api.Node;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Georgia Grigoriadou
 */

public class ClusterImpl implements Cluster {
    private String     id;
    private Set<Node>  nodes;
    private float      intraEdges;       // The internal edges of a CLUSTER
    private float      interEdges;       // The edges between two distinct clusters, where one of them is this CLUSTER
    private float      clusterFactor;    // CF = (2*intraEdges)/(2*intraEdges + interedges)

    public ClusterImpl() {
        this.id            = "0";
        this.nodes         = new HashSet<>();
        this.interEdges    = 0;
        this.intraEdges    = 0;
        this.clusterFactor = 0;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Set<Node> getNodes() {
        return nodes;
    }

    @Override
    public void setNodes(Set<Node> nodes) {
        this.nodes = nodes;
    }

    @Override
    public void addNode(Node node) {
        // Add node in Cluster
        this.nodes.add(node);

        // Update intraEdges and interedges
        node.getNeighbours().entrySet().stream()
                .filter(neighbour -> this.nodes.contains(neighbour.getKey()))
                .forEach(neighbour -> {
                    this.intraEdges += neighbour.getValue();
                    this.interEdges -= neighbour.getValue();
                });
        node.getNeighbours().entrySet().stream()
                .filter(neighbour -> !this.nodes.contains(neighbour.getKey()))
                .forEach(neighbour -> this.interEdges += neighbour.getValue()
                );

        // Update Cluster Factor
        this.calculateClusterFactor();
    }

    @Override
    public void removeNode(Node node) {
        // Remove node from Cluster
        this.nodes.remove(node);

        // Update intraEdges and interedges
        node.getNeighbours().entrySet().stream()
                .filter(neighbour -> this.nodes.contains(neighbour.getKey()))
                .forEach(neighbour -> {
                    this.intraEdges -= neighbour.getValue();
                    this.interEdges += neighbour.getValue();
                });
        node.getNeighbours().entrySet().stream()
                .filter(neighbour -> !this.nodes.contains(neighbour.getKey()))
                .forEach(neighbour -> this.interEdges -= neighbour.getValue()
                );

        // Update Cluster Factor
        this.calculateClusterFactor();
    }

    @Override
    public void setIntraEdges(float intraEdges) {
        this.intraEdges = intraEdges;
    }

    @Override
    public float getIntraEdges() {
        return intraEdges;
    }

    @Override
    public void setInterEdges(float interEdges) {
        this.interEdges = interEdges;
    }

    @Override
    public float getInterEdges() {
        return interEdges;
    }

    @Override
    public float getClusterFactor() {
        return clusterFactor;
    }

    @Override
    public void setClusterFactor(float clusterFactor) {
        this.clusterFactor = clusterFactor;
    }

    @Override
    public void calculateClusterFactor() {
        if (intraEdges == 0) {
            this.clusterFactor = 0;
            return;
        }

        this.clusterFactor = (2 * intraEdges) / ((2 * intraEdges) + interEdges);
    }

    @Override
    public Cluster clone() {
        Cluster cluster = new ClusterImpl();

        cluster.setNodes(new HashSet<>(this.nodes));
        cluster.setId(this.id);
        cluster.setInterEdges(this.interEdges);
        cluster.setIntraEdges(this.intraEdges);
        cluster.setClusterFactor(this.clusterFactor);

        return cluster;
    }

}
