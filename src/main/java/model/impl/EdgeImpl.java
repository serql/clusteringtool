package model.impl;

import model.api.Edge;

/**
 * @author Georgia Grigoriadou
 */

public class EdgeImpl implements Edge {
    private int    sourceID;
    private int    targetID;
    private String type;
    private float  weight;

    public EdgeImpl() {}

    public EdgeImpl(int sourceID, int targetID, String type, float weight) {
        this.sourceID = sourceID;
        this.targetID = targetID;
        this.type     = type;
        this.weight   = weight;
    }



    @Override
    public int getSourceID() {
        return sourceID;
    }

    @Override
    public void setSourceID(int sourceID) {
        this.sourceID = sourceID;
    }

    @Override
    public int getTargetID() {
        return targetID;
    }

    @Override
    public void setTargetID(int targetID) {
        this.targetID = targetID;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public float getWeight() {
        return weight;
    }

    @Override
    public void setWeight(float weight) {
        this.weight = weight;
    }
}
