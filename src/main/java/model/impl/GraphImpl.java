package model.impl;

import model.api.Edge;
import model.api.Graph;
import model.api.Node;

import java.util.Set;

/**
 * @author Georgia Grigoriadou
 */

public class GraphImpl implements Graph {
    private Set<Node> nodes;
    private Set<Edge> edges;

    @Override
    public Set<Node> getNodes() {
        return nodes;
    }

    @Override
    public void setNodes(Set<Node> nodes) {
        this.nodes = nodes;
    }

    @Override
    public Set<Edge> getEdges() {
        return edges;
    }

    @Override
    public void setEdges(Set<Edge> edges) {
        this.edges = edges;
    }

}
