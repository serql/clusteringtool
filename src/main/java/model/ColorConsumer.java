package model;

import java.awt.*;

/**
 * @author Georgia Grigoriadou
 */

public interface ColorConsumer {

    void accept(Color color);

}
