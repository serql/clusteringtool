package model.api;

import java.util.Set;

/**
 * @author Georgia Grigoriadou
 */

public interface Graph {

    Set<Node> getNodes();

    void setNodes(Set<Node> nodes);

    Set<Edge> getEdges();

    void setEdges(Set<Edge> edges);

}
