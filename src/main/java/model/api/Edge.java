package model.api;

/**
 * @author Georgia Grigoriadou
 */

public interface Edge {

    int getSourceID();

    void setSourceID(int sourceID);

    int getTargetID();

    void setTargetID(int targetID);

    String getType();

    void setType(String type);

    float getWeight();

    void setWeight(float weight);

}
