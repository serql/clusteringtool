package model.api;

import java.util.Set;

/**
 * @author Georgia Grigoriadou
 */

public interface Cluster {

    String getId();

    void setId(String id);

    Set<Node> getNodes();

    void setNodes(Set<Node> nodes);

    void addNode(Node node);

    void removeNode(Node node);

    void setIntraEdges(float intraEdges);

    void setInterEdges(float interEdges);

    float getIntraEdges();

    float getInterEdges();

    float getClusterFactor();

    void setClusterFactor(float clusterFactor);

    void calculateClusterFactor();

    Cluster clone();

}
