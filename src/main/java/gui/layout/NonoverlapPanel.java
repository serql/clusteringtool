package gui.layout;

import controller.visualization.Layout;
import gui.components.CollapsibleTable;
import gui.components.CustomTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

class NonoverlapPanel extends LayoutPanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Nonoverlap");

    private JTable table;

    NonoverlapPanel() {
        super();

        initTable();

        startButton.addActionListener(
                e -> {
                    try {
                        if (table.isEditing()) table.getCellEditor().stopCellEditing();

                        double speed  = Double.parseDouble((String) table.getModel().getValueAt(0, 1));
                        double ratio  = Double.parseDouble((String) table.getModel().getValueAt(1, 1));
                        double margin = Double.parseDouble((String) table.getModel().getValueAt(2, 1));

                        startLayout(Layout.getNonoverlapLayout(speed, ratio, margin));
                    } catch (NumberFormatException ex) {
                        showErrorMessage(ex);
                    }
                }
        );
        infoLabel.setToolTipText(bundle.getString("description"));
    }

    private void initTable() {
        table = new CustomTable() {
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                String tip   = null;
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                if (colIndex == 1) {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } else {
                    switch (rowIndex) {
                        case 0:
                            tip = bundle.getString("nonoverlap.speed.desc");
                            break;
                        case 1:
                            tip = bundle.getString("nonoverlap.ratio.desc");
                            break;
                        case 2:
                            tip = bundle.getString("nonoverlap.margin.desc");
                            break;
                    }
                }
                return tip;
            }
        };

        DefaultTableModel model = (DefaultTableModel) table.getModel();

        Object[] row  = new Object[2];
        row[0]        = bundle.getString("nonoverlap.speed.name");
        row[1]        = "3.0";
        Object[] row1 = new Object[2];
        row1[0]       = bundle.getString("nonoverlap.ratio.name");
        row1[1]       = "1.2";
        Object[] row2 = new Object[2];
        row2[0]       = bundle.getString("nonoverlap.margin.name");
        row2[1]       = "5.0";

        model.addRow(row);
        model.addRow(row1);
        model.addRow(row2);

        CollapsibleTable collapsibleTable = new CollapsibleTable(bundle.getString("name"), table);
        tablePanel.add(collapsibleTable);
    }

}
