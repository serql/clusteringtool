package gui.layout;

import controller.visualization.Layout;
import gui.components.CollapsibleTable;
import gui.components.CustomTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

class RandomPanel extends LayoutPanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Random");

    private JTable table;

    RandomPanel(){
        super();

        initTable();

        startButton.addActionListener(
                e -> {
                    try {
                        if (table.isEditing()) table.getCellEditor().stopCellEditing();
                        int size = Integer.parseInt((String) table.getModel().getValueAt(0, 1));
                        startLayout(Layout.getRandomLayout(size));
                    } catch (NumberFormatException ex) {
                        showErrorMessage(ex);
                    }
                }
        );
        infoLabel.setToolTipText(bundle.getString("Random.description"));
    }

    private void initTable(){
        table = new CustomTable() {
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                return colIndex == 1 ? getValueAt(rowIndex, colIndex).toString(): bundle.getString("Random.spaceSize.desc");
            }
        };

        DefaultTableModel model = (DefaultTableModel) table.getModel();

        Object[] row = new Object[2];
        row[0]       = bundle.getString("Random.spaceSize.name");
        row[1]       = "50";

        model.addRow(row);
        CollapsibleTable collapsibleTable = new CollapsibleTable(bundle.getString("Random.properties"), table);
        tablePanel.add(collapsibleTable);
    }

}
