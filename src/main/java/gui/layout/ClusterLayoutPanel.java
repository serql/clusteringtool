package gui.layout;

import controller.visualization.Layout;
import gui.components.CollapsibleTable;
import gui.components.CustomTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

class ClusterLayoutPanel extends LayoutPanel {
    ResourceBundle bundle = ResourceBundle.getBundle("Bundle/ClusterLayout");
    JTable table;


    ClusterLayoutPanel() {
        super();
        initTable();

        startButton.addActionListener(
                e -> {
                    try {
                        if (table.isEditing()) table.getCellEditor().stopCellEditing();
                        double margin = Double.parseDouble((String) table.getModel().getValueAt(0, 1));
                        startLayout(Layout.getClusterLayout(margin));
                    } catch (NumberFormatException ex) {
                        showErrorMessage(ex);
                    }
                }
        );
        infoLabel.setToolTipText(bundle.getString("description"));
    }
    private void initTable() {
        table = new CustomTable() {
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                return colIndex == 1 ? getValueAt(rowIndex, colIndex).toString(): bundle.getString("margin.desc");
            }
        };

        Object[] row = new Object[2];
        row[0]       = bundle.getString("margin.name");
        row[1]       = "5.0";
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.addRow(row);
        CollapsibleTable collapsibleTable = new CollapsibleTable(bundle.getString("properties"), table);
        tablePanel.add(collapsibleTable);
    }

}
