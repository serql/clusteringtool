package gui.layout;

import controller.visualization.Layout;
import gui.components.CollapsibleTable;
import gui.components.CustomTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

class YifanHuPanel extends LayoutPanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/YifanHu");

    private JTable yifanHuTable;
    private JTable barnesHutTable;

    YifanHuPanel(boolean proportional) {
        super();

        initTable();

        startButton.addActionListener(
                e -> {
                    try {
                        if (yifanHuTable.isEditing()) yifanHuTable.getCellEditor().stopCellEditing();
                        if (barnesHutTable.isEditing()) yifanHuTable.getCellEditor().stopCellEditing();

                        float[] floatValues = new float[6];
                        boolean adaptiveCooling;
                        int quadTreeMaxLvl;

                        floatValues[0]  = Float.parseFloat((String) yifanHuTable.getModel().getValueAt(0, 1));
                        floatValues[1]  = Float.parseFloat((String) yifanHuTable.getModel().getValueAt(1, 1));
                        floatValues[2]  = Float.parseFloat((String) yifanHuTable.getModel().getValueAt(2, 1));
                        floatValues[3]  = Float.parseFloat((String) yifanHuTable.getModel().getValueAt(3, 1));
                        floatValues[4]  = Float.parseFloat((String) yifanHuTable.getModel().getValueAt(5, 1));
                        floatValues[5]  = Float.parseFloat((String) barnesHutTable.getModel().getValueAt(1, 1));
                        adaptiveCooling = (boolean) yifanHuTable.getModel().getValueAt(4, 1);
                        quadTreeMaxLvl  = Integer.parseInt((String) barnesHutTable.getModel().getValueAt(0, 1));

                        if(proportional) {
                            startLayout(Layout.getYifanHuProportionalLayout(floatValues, adaptiveCooling, quadTreeMaxLvl));
                        } else {
                            startLayout(Layout.getYifanHuLayout(floatValues, adaptiveCooling, quadTreeMaxLvl));
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(ex);
                    }
                }
        );

        if (proportional) {
            infoLabel.setToolTipText(bundle.getString("YifanHu.description"));
        } else {
            infoLabel.setToolTipText(bundle.getString("YifanHuProportional.description"));
        }
    }

    private void initTable() {
        yifanHuTable = new CustomTable() {
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                String tip   = null;
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                if (colIndex == 1) {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } else {
                    switch (rowIndex) {
                        case 0:
                            tip = bundle.getString("YifanHu.optimalDistance.desc");
                            break;
                        case 1:
                            tip = bundle.getString("YifanHu.relativeStrength.desc");
                            break;
                        case 2:
                            tip = bundle.getString("YifanHu.initialStepSize.desc");
                            break;
                        case 3:
                            tip = bundle.getString("YifanHu.stepRatio.desc");
                            break;
                        case 4:
                            tip = bundle.getString("YifanHu.adaptativeCooling.desc");
                            break;
                        case 5:
                            tip = bundle.getString("YifanHu.convergenceThreshold.desc");
                            break;
                    }
                }
                return tip;
            }
        };
        barnesHutTable = new CustomTable() {
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                String tip   = null;
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                if (colIndex == 1) {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } else {
                    switch (rowIndex) {
                        case 0:
                            tip = bundle.getString("YifanHu.quadTreeMaxLevel.desc");
                            break;
                        case 1:
                            tip = bundle.getString("YifanHu.theta.desc");
                            break;
                    }
                }
                return tip;
            }
        };

        DefaultTableModel yifanHuModel   = (DefaultTableModel) yifanHuTable.getModel();
        DefaultTableModel barnesHutModel = (DefaultTableModel) barnesHutTable.getModel();

        Object[] row  = new Object[2];
        row[0]        = bundle.getString("YifanHu.optimalDistance.name");
        row[1]        = "100.0";
        Object[] row1 = new Object[2];
        row1[0]       = bundle.getString("YifanHu.relativeStrength.name");
        row1[1]       = "0.2";
        Object[] row2 = new Object[2];
        row2[0]       = bundle.getString("YifanHu.initialStepSize.name");
        row2[1]       = "20.0";
        Object[] row3 = new Object[2];
        row3[0]       = bundle.getString("YifanHu.stepRatio.name");
        row3[1]       = "0.95";
        Object[] row4 = new Object[2];
        row4[0]       = bundle.getString("YifanHu.adaptativeCooling.name");
        row4[1]       = true;
        Object[] row5 = new Object[2];
        row5[0]       = bundle.getString("YifanHu.convergenceThreshold.name");
        row5[1]       = "1.0E-4";
        Object[] row6 = new Object[2];
        row6[0]       = bundle.getString("YifanHu.quadTreeMaxLevel.name");
        row6[1]       = "10";
        Object[] row7 = new Object[2];
        row7[0]       = bundle.getString("YifanHu.theta.name");
        row7[1]       = "1.2";


        yifanHuModel.addRow(row);
        yifanHuModel.addRow(row1);
        yifanHuModel.addRow(row2);
        yifanHuModel.addRow(row3);
        yifanHuModel.addRow(row4);
        yifanHuModel.addRow(row5);
        barnesHutModel.addRow(row6);
        barnesHutModel.addRow(row7);

        CollapsibleTable yifanHuCollapsibleTable   = new CollapsibleTable(bundle.getString("YifanHu.yifanHuProperties"), yifanHuTable);
        CollapsibleTable barnesHutCollapsibleTable = new CollapsibleTable(bundle.getString("YifanHu.barnesHutproperties"), barnesHutTable);

        tablePanel.add(yifanHuCollapsibleTable);
        tablePanel.add(barnesHutCollapsibleTable);
    }

}
