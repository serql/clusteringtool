package gui.layout;

import controller.visualization.Preview;
import gui.components.SubtleSquareBorder;
import org.jfree.ui.tabbedui.VerticalLayout;

import javax.swing.*;
import java.awt.*;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

class LayoutPanel extends JPanel {
    JLabel  infoLabel;
    JButton startButton;
    JPanel  tablePanel;

    private JButton stopButton;

    ResourceBundle  bundle = ResourceBundle.getBundle("Bundle/Layout");

    LayoutPanel(){
        super();
        initComponents();
    }

    private void initComponents(){

        JScrollPane optionPanel = new JScrollPane();
        JPanel headerPanel      = new JPanel();
        JPanel buttonPanel      = new JPanel();

        infoLabel   = new JLabel();
        startButton = new JButton();
        stopButton  = new JButton();
        tablePanel  = new JPanel();

        optionPanel.setBorder(new SubtleSquareBorder(true));

        startButton.setText(bundle.getString("Layout.start"));
        stopButton.setText(bundle.getString("Layout.stop"));

        infoLabel.setIcon(new ImageIcon(bundle.getString("Layout.infoIconPath")));
        startButton.setIcon(new ImageIcon(bundle.getString("Layout.playIconPath")));
        stopButton.setIcon(new ImageIcon(bundle.getString("Layout.stopIconPath")));

        stopButton.setVisible(false);

        setLayout(new BorderLayout());
        headerPanel.setLayout(new BorderLayout());
        buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT,0,0));
        headerPanel.add(infoLabel,BorderLayout.WEST);
        headerPanel.add(buttonPanel,BorderLayout.EAST);
        buttonPanel.add(startButton);
        buttonPanel.add(stopButton);
        tablePanel.setLayout(new VerticalLayout());
        optionPanel.setViewportView(tablePanel);
        add(headerPanel, BorderLayout.NORTH);
        add(optionPanel, BorderLayout.CENTER);
    }

     void startLayout(org.gephi.layout.spi.Layout layout) {
         startButton.setVisible(false);
         startButton.firePropertyChange("visible", true, false);
         stopButton.setVisible(true);

         SwingWorker worker = new SwingWorker<Void, Void>() {
             @Override
             public Void doInBackground() {
                 layout.initAlgo();

                 while(layout.canAlgo()  && !isCancelled()) {
                     layout.goAlgo();
                     try{
                         Thread.sleep(5);
                         Preview.refreshPreview();
                     } catch (InterruptedException ignored) {}
                 }

                 layout.endAlgo();
                 Preview.refreshPreview();

                 startButton.setVisible(true);
                 startButton.firePropertyChange("visible", false, true);
                 stopButton.setVisible(false);

                 Preview.refreshPreview();
                 return null;
             }
         };
         stopButton.addActionListener(e -> worker.cancel(true));
         worker.execute();
     }

    void showErrorMessage(Exception e){
        JOptionPane.showMessageDialog(null, bundle.getString("Layout.nonLegalValue") + e.getMessage().substring(19, e.getMessage().length()-1));
    }

}
