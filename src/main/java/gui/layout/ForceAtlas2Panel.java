package gui.layout;

import controller.visualization.Layout;
import gui.components.CollapsibleTable;
import gui.components.CustomTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

class ForceAtlas2Panel extends LayoutPanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/ForceAtlas2");

    private JTable threadTable;
    private JTable performanceTable;
    private JTable tuningTable;
    private JTable behaviorTable;

    ForceAtlas2Panel() {
        super();

        initTable();

        startButton.addActionListener(
                e -> {
                    try {
                        if (threadTable.isEditing()) threadTable.getCellEditor().stopCellEditing();
                        if (performanceTable.isEditing()) performanceTable.getCellEditor().stopCellEditing();
                        if (tuningTable.isEditing()) tuningTable.getCellEditor().stopCellEditing();
                        if (behaviorTable.isEditing()) behaviorTable.getCellEditor().stopCellEditing();

                        int threads;
                        double[] doubleValues   = new double[6];
                        boolean[] booleanValues = new boolean[5];

                        threads          = Integer.parseInt((String) threadTable.getModel().getValueAt(0, 1));
                        doubleValues[0]  = Double.parseDouble((String) performanceTable.getModel().getValueAt(0, 1));
                        doubleValues[1]  = Double.parseDouble((String) performanceTable.getModel().getValueAt(2, 1));
                        doubleValues[2]  = Double.parseDouble((String) tuningTable.getModel().getValueAt(0, 1));
                        doubleValues[3]  = Double.parseDouble((String) tuningTable.getModel().getValueAt(2, 1));
                        doubleValues[4]  = Double.parseDouble((String) behaviorTable.getModel().getValueAt(3, 1));
                        booleanValues[0] = (boolean) performanceTable.getModel().getValueAt(1, 1);
                        booleanValues[1] = (boolean) tuningTable.getModel().getValueAt(1, 1);
                        booleanValues[2] = (boolean) behaviorTable.getModel().getValueAt(0, 1);
                        booleanValues[3] = (boolean) behaviorTable.getModel().getValueAt(1, 1);
                        booleanValues[4] = (boolean) behaviorTable.getModel().getValueAt(2, 1);

                       startLayout(Layout.getForceAtlas2Layout(threads, doubleValues, booleanValues));
                    } catch (NumberFormatException ex) {
                        showErrorMessage(ex);
                    }
                }
        );
        infoLabel.setToolTipText(bundle.getString("ForceAtlas2.description"));
    }

    private void initTable() {
        threadTable = new CustomTable() {
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                String tip;
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                if (colIndex == 1) {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } else {
                    tip = bundle.getString("ForceAtlas2.threads.desc");
                }
                return tip;
            }
        };
        performanceTable = new CustomTable() {
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                String tip   = null;
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                if (colIndex == 1) {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } else {
                    switch (rowIndex) {
                        case 0:
                            tip = bundle.getString("ForceAtlas2.jitterTolerance.desc");
                            break;
                        case 1:
                            tip = bundle.getString("ForceAtlas2.barnesHutOptimization.desc");
                            break;
                        case 2:
                            tip = bundle.getString("ForceAtlas2.barnesHutTheta.desc");
                            break;
                    }
                }
                return tip;
            }
        };
        tuningTable = new CustomTable() {
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                String tip   = null;
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                if (colIndex == 1) {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } else {
                    switch (rowIndex) {
                        case 0:
                            tip = bundle.getString("ForceAtlas2.scalingRatio.desc");
                            break;
                        case 1:
                            tip = bundle.getString("ForceAtlas2.strongGravityMode.desc");
                            break;
                        case 2:
                            tip = bundle.getString("ForceAtlas2.gravity.desc");
                            break;
                    }
                }
                return tip;
            }
        };
        behaviorTable = new CustomTable() {
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                String tip   = null;
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                if (colIndex == 1) {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } else {
                    switch (rowIndex) {
                        case 0:
                            tip = bundle.getString("ForceAtlas2.distributedAttraction.desc");
                            break;
                        case 1:
                            tip = bundle.getString("ForceAtlas2.linLogMode.desc");
                            break;
                        case 2:
                            tip = bundle.getString("ForceAtlas2.adjustSizes.desc");
                            break;
                        case 3:
                            tip = bundle.getString("ForceAtlas2.edgeWeightInfluence.desc");
                            break;
                    }
                }
                return tip;
            }
        };

        DefaultTableModel threadModel      = (DefaultTableModel) threadTable.getModel();
        DefaultTableModel performanceModel = (DefaultTableModel) performanceTable.getModel();
        DefaultTableModel tuningModel      = (DefaultTableModel) tuningTable.getModel();
        DefaultTableModel behaviorModel    = (DefaultTableModel) behaviorTable.getModel();

        Object[] row   = new Object[2];
        row[0]         = bundle.getString("ForceAtlas2.threads.name");
        row[1]         = "7";
        Object[] row1  = new Object[2];
        row1[0]        = bundle.getString("ForceAtlas2.jitterTolerance.name");
        row1[1]        = "1.0";
        Object[] row2  = new Object[2];
        row2[0]        = bundle.getString("ForceAtlas2.barnesHutOptimization.name");
        row2[1]        = false;
        Object[] row3  = new Object[2];
        row3[0]        = bundle.getString("ForceAtlas2.barnesHutTheta.name");
        row3[1]        = "1.2";
        Object[] row4  = new Object[2];
        row4[0]        = bundle.getString("ForceAtlas2.scalingRatio.name");
        row4[1]        = "10.0";
        Object[] row5  = new Object[2];
        row5[0]        = bundle.getString("ForceAtlas2.strongGravityMode.name");
        row5[1]        = false;
        Object[] row6  = new Object[2];
        row6[0]        = bundle.getString("ForceAtlas2.gravity.name");
        row6[1]        = "1.0";
        Object[] row7  = new Object[2];
        row7[0]        = bundle.getString("ForceAtlas2.distributedAttraction.name");
        row7[1]        = false;
        Object[] row8  = new Object[2];
        row8[0]        = bundle.getString("ForceAtlas2.linLogMode.name");
        row8[1]        = false;
        Object[] row9  = new Object[2];
        row9[0]        = bundle.getString("ForceAtlas2.adjustSizes.name");
        row9[1]        = false;
        Object[] row10 = new Object[2];
        row10[0]       = bundle.getString("ForceAtlas2.edgeWeightInfluence.name");
        row10[1]       = "1.0";

        threadModel.addRow(row);
        performanceModel.addRow(row1);
        performanceModel.addRow(row2);
        performanceModel.addRow(row3);
        tuningModel.addRow(row4);
        tuningModel.addRow(row5);
        tuningModel.addRow(row6);
        behaviorModel.addRow(row7);
        behaviorModel.addRow(row8);
        behaviorModel.addRow(row9);
        behaviorModel.addRow(row10);

        CollapsibleTable threadCollapsibleTable      = new CollapsibleTable(bundle.getString("ForceAtlas2.threads"), threadTable);
        CollapsibleTable performanceCollapsibleTable = new CollapsibleTable(bundle.getString("ForceAtlas2.performance"), performanceTable);
        CollapsibleTable tuningCollapsibleTable      = new CollapsibleTable(bundle.getString("ForceAtlas2.tuning"), tuningTable);
        CollapsibleTable behaviorCollapsibleTable    = new CollapsibleTable(bundle.getString("ForceAtlas2.behavior"), behaviorTable);

        tablePanel.add(threadCollapsibleTable);
        tablePanel.add(performanceCollapsibleTable);
        tablePanel.add(tuningCollapsibleTable);
        tablePanel.add(behaviorCollapsibleTable);
    }

}
