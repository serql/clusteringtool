package gui.layout;

import controller.visualization.Layout;

/**
 * @author Gewrgia
 */

class ContractionPanel extends ScalePanel {
    ContractionPanel() {
        super("0.8");

        startButton.addActionListener(
                e -> {
                    try {
                        if (table.isEditing()) table.getCellEditor().stopCellEditing();
                        double scaleFactor = Double.parseDouble((String) table.getModel().getValueAt(0, 1));
                        startLayout(Layout.getContractLayout(scaleFactor));
                    } catch (NumberFormatException ex) {
                        showErrorMessage(ex);
                    }
                }
        );
        infoLabel.setToolTipText(bundle.getString("contract.description"));
    }

}
