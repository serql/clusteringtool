package gui.dataLaboratory;

import controller.visualization.GephiInitializer;
import gui.resources.ResourceBundleList;
import org.gephi.graph.api.DirectedGraph;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import controller.Project;

/**
 * @author Gewrgia
 */

class NodesTablePanel extends TablePanel{

    NodesTablePanel(){}

    @Override
    void init(){

        initTable( () -> {
            createTableModel(ResourceBundleList.getPropertyStringArray(bundle, "Table.nodeColumns"));

            GraphModel graphModel = Project.getGraphModel();
            DirectedGraph directedGraph = graphModel.getDirectedGraph();
            for (Node node : directedGraph.getNodes()) {
                Object[] row = new Object[6];

                row[0] = node.getId();
                row[1] = node.getLabel();
                row[2] = node.getAttribute(GephiInitializer.CLUSTER);
                row[3] = directedGraph.getDegree(node);
                row[4] = directedGraph.getInDegree(node);
                row[5] = directedGraph.getOutDegree(node);

                tableModel.addRow(row);
            }
        });
    }

    @Override
    void rename(String oldID, String newID) {
        for (int i = 0; i < tableModel.getRowCount(); i++) {
            String oldValue = (String) tableModel.getValueAt(i, 2);
            if (oldValue.equals(oldID)) {
                tableModel.setValueAt(newID, i, 2);
            }
        }
    }

}
