package gui.components;

import com.bric.colorpicker.ColorPicker;
import model.ColorConsumer;

import javax.swing.*;
import java.awt.*;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Georgia Grigoriadou
 */

public class CustomColorPicker extends JDialog {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Components");

    private ColorPicker   colorPicker;
    private ColorConsumer consumer;

    public CustomColorPicker(boolean showExpertControls, boolean includeOpacity, ColorConsumer colorConsumer){
        super();
        setTitle(bundle.getString("Components.selectColor"));
        consumer    = colorConsumer;
        colorPicker = new ColorPicker(showExpertControls, includeOpacity);
        initComponents();
    }

    private void initComponents(){
        JPanel buttonPanel   = new JPanel();
        JButton okButton     = new JButton(bundle.getString("Components.ok"));
        JButton cancelButton = new JButton(bundle.getString("Components.cancel"));

        AtomicReference<Color> colorRef = new AtomicReference<>(null);
        colorPicker.setColor(Color.WHITE);
        colorPicker.addColorListener(colorModel -> colorRef.set(colorModel.getColor()));
        cancelButton.addActionListener(event -> dispose());
        okButton.addActionListener(event -> {
                    if(colorRef.get() !=null) {
                        consumer.accept(colorRef.get());
                    }
                    dispose();
                }
        );

        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        setLayout(new BorderLayout());
        add(colorPicker, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
        setSize(400,350);
        setLocationRelativeTo(null);
        setModal(true);
        setVisible(true);
    }

}
