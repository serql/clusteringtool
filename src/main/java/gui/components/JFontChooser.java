package gui.components;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import java.awt.*;
import java.awt.event.*;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

@SuppressWarnings("all")
public class JFontChooser extends JComponent {
    public static final int OK_OPTION = 0;
    public static final int CANCEL_OPTION = 1;
    public static final int ERROR_OPTION = -1;
    private static Font DEFAULT_SELECTED_FONT = new Font("Serif", 0, 12);
    private static final Font DEFAULT_FONT = new Font("Dialog", 0, 10);
    private static final int[] FONT_STYLE_CODES = new int[]{0, 1, 2, 3};
    private static final String[] DEFAULT_FONT_SIZE_STRINGS = new String[]{"8", "9", "10", "11", "12", "14", "16", "18", "20", "22", "24", "26", "28", "36", "48", "72"};
    protected int dialogResultValue;
    private ResourceBundle messageCatalog;
    private String[] fontStyleNames;
    private String[] fontFamilyNames;
    private String[] fontSizeStrings;
    private JTextField fontFamilyTextField;
    private JTextField fontStyleTextField;
    private JTextField fontSizeTextField;
    private JList fontNameList;
    private JList fontStyleList;
    private JList fontSizeList;
    private JPanel fontNamePanel;
    private JPanel fontStylePanel;
    private JPanel fontSizePanel;
    private JPanel samplePanel;
    private JTextField sampleText;

    protected String _(String key) {
        String value = key;

        try {
//          value = this.messageCatalog.getString(key);
        } catch (MissingResourceException var4) {
            ;
        }

        return value;
    }

    public JFontChooser() {
        this(DEFAULT_FONT_SIZE_STRINGS);
    }

    public JFontChooser(String[] fontSizeStrings) {
        this.dialogResultValue = -1;
//      this.messageCatalog = ResourceBundle.getBundle(JFontChooser.class.getName() + "Messages", this.getLocale());
        this.fontStyleNames = null;
        this.fontFamilyNames = null;
        this.fontSizeStrings = null;
        this.fontFamilyTextField = null;
        this.fontStyleTextField = null;
        this.fontSizeTextField = null;
        this.fontNameList = null;
        this.fontStyleList = null;
        this.fontSizeList = null;
        this.fontNamePanel = null;
        this.fontStylePanel = null;
        this.fontSizePanel = null;
        this.samplePanel = null;
        this.sampleText = null;
        if(fontSizeStrings == null) {
            fontSizeStrings = DEFAULT_FONT_SIZE_STRINGS;
        }

        this.fontSizeStrings = fontSizeStrings;
        JPanel selectPanel = new JPanel();
        selectPanel.setLayout(new BoxLayout(selectPanel, 0));
        selectPanel.add(this.getFontFamilyPanel());
        selectPanel.add(this.getFontStylePanel());
        selectPanel.add(this.getFontSizePanel());
        JPanel contentsPanel = new JPanel();
        contentsPanel.setLayout(new GridLayout(2, 1));
        contentsPanel.add(selectPanel, "North");
        contentsPanel.add(this.getSamplePanel(), "Center");
        this.setLayout(new BoxLayout(this, 0));
        this.add(contentsPanel);
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.setSelectedFont(DEFAULT_SELECTED_FONT);
    }

    public static Font getDefaultSelectedFont() {
        return DEFAULT_SELECTED_FONT;
    }

    public static void setDefaultSelectedFont(Font defaultSelectedFont) {
        DEFAULT_SELECTED_FONT = defaultSelectedFont;
    }

    public JTextField getFontFamilyTextField() {
        if(this.fontFamilyTextField == null) {
            this.fontFamilyTextField = new JTextField();
            this.fontFamilyTextField.addFocusListener(new JFontChooser.TextFieldFocusHandlerForTextSelection(this.fontFamilyTextField));
            this.fontFamilyTextField.addKeyListener(new JFontChooser.TextFieldKeyHandlerForListSelectionUpDown(this.getFontFamilyList()));
            this.fontFamilyTextField.getDocument().addDocumentListener(new JFontChooser.ListSearchTextFieldDocumentHandler(this.getFontFamilyList()));
            this.fontFamilyTextField.setFont(DEFAULT_FONT);
        }

        return this.fontFamilyTextField;
    }

    public JTextField getFontStyleTextField() {
        if(this.fontStyleTextField == null) {
            this.fontStyleTextField = new JTextField();
            this.fontStyleTextField.addFocusListener(new JFontChooser.TextFieldFocusHandlerForTextSelection(this.fontStyleTextField));
            this.fontStyleTextField.addKeyListener(new JFontChooser.TextFieldKeyHandlerForListSelectionUpDown(this.getFontStyleList()));
            this.fontStyleTextField.getDocument().addDocumentListener(new JFontChooser.ListSearchTextFieldDocumentHandler(this.getFontStyleList()));
            this.fontStyleTextField.setFont(DEFAULT_FONT);
        }

        return this.fontStyleTextField;
    }

    public JTextField getFontSizeTextField() {
        if(this.fontSizeTextField == null) {
            this.fontSizeTextField = new JTextField();
            this.fontSizeTextField.addFocusListener(new JFontChooser.TextFieldFocusHandlerForTextSelection(this.fontSizeTextField));
            this.fontSizeTextField.addKeyListener(new JFontChooser.TextFieldKeyHandlerForListSelectionUpDown(this.getFontSizeList()));
            this.fontSizeTextField.getDocument().addDocumentListener(new JFontChooser.ListSearchTextFieldDocumentHandler(this.getFontSizeList()));
            this.fontSizeTextField.setFont(DEFAULT_FONT);
        }

        return this.fontSizeTextField;
    }

    public JList getFontFamilyList() {
        if(this.fontNameList == null) {
            this.fontNameList = new JList(this.getFontFamilies());
            this.fontNameList.setSelectionMode(0);
            this.fontNameList.addListSelectionListener(new JFontChooser.ListSelectionHandler(this.getFontFamilyTextField()));
            this.fontNameList.setSelectedIndex(0);
            this.fontNameList.setFont(DEFAULT_FONT);
            this.fontNameList.setFocusable(false);
        }

        return this.fontNameList;
    }

    public JList getFontStyleList() {
        if(this.fontStyleList == null) {
            this.fontStyleList = new JList(this.getFontStyleNames());
            this.fontStyleList.setSelectionMode(0);
            this.fontStyleList.addListSelectionListener(new JFontChooser.ListSelectionHandler(this.getFontStyleTextField()));
            this.fontStyleList.setSelectedIndex(0);
            this.fontStyleList.setFont(DEFAULT_FONT);
            this.fontStyleList.setFocusable(false);
        }

        return this.fontStyleList;
    }

    public JList getFontSizeList() {
        if(this.fontSizeList == null) {
            this.fontSizeList = new JList(this.fontSizeStrings);
            this.fontSizeList.setSelectionMode(0);
            this.fontSizeList.addListSelectionListener(new JFontChooser.ListSelectionHandler(this.getFontSizeTextField()));
            this.fontSizeList.setSelectedIndex(0);
            this.fontSizeList.setFont(DEFAULT_FONT);
            this.fontSizeList.setFocusable(false);
        }

        return this.fontSizeList;
    }

    public String getSelectedFontFamily() {
        String fontName = (String)this.getFontFamilyList().getSelectedValue();
        return fontName;
    }

    public int getSelectedFontStyle() {
        int index = this.getFontStyleList().getSelectedIndex();
        return FONT_STYLE_CODES[index];
    }

    public int getSelectedFontSize() {
        boolean fontSize = true;
        String fontSizeString = this.getFontSizeTextField().getText();

        while(true) {
            try {
                int fontSize1 = Integer.parseInt(fontSizeString);
                return fontSize1;
            } catch (NumberFormatException var4) {
                fontSizeString = (String)this.getFontSizeList().getSelectedValue();
                this.getFontSizeTextField().setText(fontSizeString);
            }
        }
    }

    public Font getSelectedFont() {
        Font font = new Font(this.getSelectedFontFamily(), this.getSelectedFontStyle(), this.getSelectedFontSize());
        return font;
    }

    public void setSelectedFontFamily(String name) {
        String[] names = this.getFontFamilies();

        for(int i = 0; i < names.length; ++i) {
            if(names[i].toLowerCase().equals(name.toLowerCase())) {
                this.getFontFamilyList().setSelectedIndex(i);
                break;
            }
        }

        this.updateSampleFont();
    }

    public void setSelectedFontStyle(int style) {
        for(int i = 0; i < FONT_STYLE_CODES.length; ++i) {
            if(FONT_STYLE_CODES[i] == style) {
                this.getFontStyleList().setSelectedIndex(i);
                break;
            }
        }

        this.updateSampleFont();
    }

    public void setSelectedFontSize(int size) {
        String sizeString = String.valueOf(size);

        for(int i = 0; i < this.fontSizeStrings.length; ++i) {
            if(this.fontSizeStrings[i].equals(sizeString)) {
                this.getFontSizeList().setSelectedIndex(i);
                break;
            }
        }

        this.getFontSizeTextField().setText(sizeString);
        this.updateSampleFont();
    }

    public void setSelectedFont(Font font) {
        this.setSelectedFontFamily(font.getFamily());
        this.setSelectedFontStyle(font.getStyle());
        this.setSelectedFontSize(font.getSize());
    }

    public String getVersionString() {
        return this._("Version");
    }

    public int showDialog(Component parent) {
        this.dialogResultValue = -1;
        JDialog dialog = this.createDialog(parent);
        dialog.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                JFontChooser.this.dialogResultValue = 1;
            }
        });
        dialog.setVisible(true);
        dialog.dispose();
        dialog = null;
        return this.dialogResultValue;
    }

    protected JDialog createDialog(Component parent) {
        Frame frame = parent instanceof Frame?(Frame)parent:(Frame)SwingUtilities.getAncestorOfClass(Frame.class, parent);
        JDialog dialog = new JDialog(frame, this._("Select Font"), true);
        JFontChooser.DialogOKAction okAction = new JFontChooser.DialogOKAction(dialog);
        JFontChooser.DialogCancelAction cancelAction = new JFontChooser.DialogCancelAction(dialog);
        JButton okButton = new JButton(okAction);
        okButton.setFont(DEFAULT_FONT);
        JButton cancelButton = new JButton(cancelAction);
        cancelButton.setFont(DEFAULT_FONT);
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(2, 1));
        buttonsPanel.add(okButton);
        buttonsPanel.add(cancelButton);
        buttonsPanel.setBorder(BorderFactory.createEmptyBorder(25, 0, 10, 10));
        ActionMap actionMap = buttonsPanel.getActionMap();
        actionMap.put(cancelAction.getValue("Default"), cancelAction);
        actionMap.put(okAction.getValue("Default"), okAction);
        InputMap inputMap = buttonsPanel.getInputMap(2);
        inputMap.put(KeyStroke.getKeyStroke("ESCAPE"), cancelAction.getValue("Default"));
        inputMap.put(KeyStroke.getKeyStroke("ENTER"), okAction.getValue("Default"));
        JPanel dialogEastPanel = new JPanel();
        dialogEastPanel.setLayout(new BorderLayout());
        dialogEastPanel.add(buttonsPanel, "North");
        dialog.getContentPane().add(this, "Center");
        dialog.getContentPane().add(dialogEastPanel, "East");
        dialog.pack();
        dialog.setLocationRelativeTo(frame);
        return dialog;
    }

    protected void updateSampleFont() {
        Font font = this.getSelectedFont();
        this.getSampleTextField().setFont(font);
    }

    protected JPanel getFontFamilyPanel() {
        if(this.fontNamePanel == null) {
            this.fontNamePanel = new JPanel();
            this.fontNamePanel.setLayout(new BorderLayout());
            this.fontNamePanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
            this.fontNamePanel.setPreferredSize(new Dimension(180, 130));
            JScrollPane scrollPane = new JScrollPane(this.getFontFamilyList());
            scrollPane.getVerticalScrollBar().setFocusable(false);
            scrollPane.setVerticalScrollBarPolicy(22);
            JPanel p = new JPanel();
            p.setLayout(new BorderLayout());
            p.add(this.getFontFamilyTextField(), "North");
            p.add(scrollPane, "Center");
            JLabel label = new JLabel(this._("FontName"));
            label.setHorizontalAlignment(2);
            label.setHorizontalTextPosition(2);
            label.setLabelFor(this.getFontFamilyTextField());
            label.setDisplayedMnemonic('F');
            this.fontNamePanel.add(label, "North");
            this.fontNamePanel.add(p, "Center");
        }

        return this.fontNamePanel;
    }

    protected JPanel getFontStylePanel() {
        if(this.fontStylePanel == null) {
            this.fontStylePanel = new JPanel();
            this.fontStylePanel.setLayout(new BorderLayout());
            this.fontStylePanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
            this.fontStylePanel.setPreferredSize(new Dimension(140, 130));
            JScrollPane scrollPane = new JScrollPane(this.getFontStyleList());
            scrollPane.getVerticalScrollBar().setFocusable(false);
            scrollPane.setVerticalScrollBarPolicy(22);
            JPanel p = new JPanel();
            p.setLayout(new BorderLayout());
            p.add(this.getFontStyleTextField(), "North");
            p.add(scrollPane, "Center");
            JLabel label = new JLabel(this._("FontStyle"));
            label.setHorizontalAlignment(2);
            label.setHorizontalTextPosition(2);
            label.setLabelFor(this.getFontStyleTextField());
            label.setDisplayedMnemonic('Y');
            this.fontStylePanel.add(label, "North");
            this.fontStylePanel.add(p, "Center");
        }

        return this.fontStylePanel;
    }

    protected JPanel getFontSizePanel() {
        if(this.fontSizePanel == null) {
            this.fontSizePanel = new JPanel();
            this.fontSizePanel.setLayout(new BorderLayout());
            this.fontSizePanel.setPreferredSize(new Dimension(70, 130));
            this.fontSizePanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
            JScrollPane scrollPane = new JScrollPane(this.getFontSizeList());
            scrollPane.getVerticalScrollBar().setFocusable(false);
            scrollPane.setVerticalScrollBarPolicy(22);
            JPanel p = new JPanel();
            p.setLayout(new BorderLayout());
            p.add(this.getFontSizeTextField(), "North");
            p.add(scrollPane, "Center");
            JLabel label = new JLabel(this._("FontSize"));
            label.setHorizontalAlignment(2);
            label.setHorizontalTextPosition(2);
            label.setLabelFor(this.getFontSizeTextField());
            label.setDisplayedMnemonic('S');
            this.fontSizePanel.add(label, "North");
            this.fontSizePanel.add(p, "Center");
        }

        return this.fontSizePanel;
    }

    protected JPanel getSamplePanel() {
        if(this.samplePanel == null) {
            TitledBorder titledBorder = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), this._("Sample"));
            Border empty = BorderFactory.createEmptyBorder(5, 10, 10, 10);
            CompoundBorder border = BorderFactory.createCompoundBorder(titledBorder, empty);
            this.samplePanel = new JPanel();
            this.samplePanel.setLayout(new BorderLayout());
            this.samplePanel.setBorder(border);
            this.samplePanel.add(this.getSampleTextField(), "Center");
        }

        return this.samplePanel;
    }

    protected JTextField getSampleTextField() {
        if(this.sampleText == null) {
            Border lowered = BorderFactory.createLoweredBevelBorder();
            this.sampleText = new JTextField(this._("SampleString"));
            this.sampleText.setBorder(lowered);
            this.sampleText.setPreferredSize(new Dimension(300, 100));
        }

        return this.sampleText;
    }

    protected String[] getFontFamilies() {
        if(this.fontFamilyNames == null) {
            GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
            this.fontFamilyNames = env.getAvailableFontFamilyNames();
        }

        return this.fontFamilyNames;
    }

    protected String[] getFontStyleNames() {
        if(this.fontStyleNames == null) {
            byte i = 0;
            this.fontStyleNames = new String[4];
            int var2 = i + 1;
            this.fontStyleNames[i] = this._("Plain");
            this.fontStyleNames[var2++] = this._("Bold");
            this.fontStyleNames[var2++] = this._("Italic");
            this.fontStyleNames[var2++] = this._("BoldItalic");
        }

        return this.fontStyleNames;
    }

    protected class DialogCancelAction extends AbstractAction {
        protected static final String ACTION_NAME = "Cancel";
        private JDialog dialog;

        protected DialogCancelAction(JDialog dialog) {
            this.dialog = dialog;
            this.putValue("Default", "Cancel");
            this.putValue("ActionCommandKey", "Cancel");
            this.putValue("Name", JFontChooser.this._("Cancel"));
        }

        public void actionPerformed(ActionEvent e) {
            JFontChooser.this.dialogResultValue = 1;
            this.dialog.setVisible(false);
        }
    }

    protected class DialogOKAction extends AbstractAction {
        protected static final String ACTION_NAME = "OK";
        private JDialog dialog;

        protected DialogOKAction(JDialog dialog) {
            this.dialog = dialog;
            this.putValue("Default", "OK");
            this.putValue("ActionCommandKey", "OK");
            this.putValue("Name", JFontChooser.this._("OK"));
        }

        public void actionPerformed(ActionEvent e) {
            JFontChooser.this.dialogResultValue = 0;
            this.dialog.setVisible(false);
        }
    }

    protected class ListSearchTextFieldDocumentHandler implements DocumentListener {
        JList targetList;

        public ListSearchTextFieldDocumentHandler(JList targetList) {
            this.targetList = targetList;
        }

        public void insertUpdate(DocumentEvent e) {
            this.update(e);
        }

        public void removeUpdate(DocumentEvent e) {
            this.update(e);
        }

        public void changedUpdate(DocumentEvent e) {
            this.update(e);
        }

        private void update(DocumentEvent event) {
            String newValue = "";

            try {
                Document index = event.getDocument();
                newValue = index.getText(0, index.getLength());
            } catch (BadLocationException var5) {
                var5.printStackTrace();
            }

            if(newValue.length() > 0) {
                int index1 = this.targetList.getNextMatch(newValue, 0, Position.Bias.Forward);
                if(index1 < 0) {
                    index1 = 0;
                }

                this.targetList.ensureIndexIsVisible(index1);
                String matchedName = this.targetList.getModel().getElementAt(index1).toString();
                if(newValue.equalsIgnoreCase(matchedName) && index1 != this.targetList.getSelectedIndex()) {
                    SwingUtilities.invokeLater(new JFontChooser.ListSearchTextFieldDocumentHandler.ListSelector(index1));
                }
            }

        }

        public class ListSelector implements Runnable {
            private int index;

            public ListSelector(int index) {
                this.index = index;
            }

            public void run() {
                JFontChooser.ListSearchTextFieldDocumentHandler.this.targetList.setSelectedIndex(this.index);
            }
        }
    }

    protected class TextFieldKeyHandlerForListSelectionUpDown extends KeyAdapter {
        private JList targetList;

        public TextFieldKeyHandlerForListSelectionUpDown(JList list) {
            this.targetList = list;
        }

        public void keyPressed(KeyEvent e) {
            int i = this.targetList.getSelectedIndex();
            switch(e.getKeyCode()) {
                case 38:
                    i = this.targetList.getSelectedIndex() - 1;
                    if(i < 0) {
                        i = 0;
                    }

                    this.targetList.setSelectedIndex(i);
                    break;
                case 40:
                    int listSize = this.targetList.getModel().getSize();
                    i = this.targetList.getSelectedIndex() + 1;
                    if(i >= listSize) {
                        i = listSize - 1;
                    }

                    this.targetList.setSelectedIndex(i);
            }

        }
    }

    protected class TextFieldFocusHandlerForTextSelection extends FocusAdapter {
        private JTextComponent textComponent;

        public TextFieldFocusHandlerForTextSelection(JTextComponent textComponent) {
            this.textComponent = textComponent;
        }

        public void focusGained(FocusEvent e) {
            this.textComponent.selectAll();
        }

        public void focusLost(FocusEvent e) {
            this.textComponent.select(0, 0);
            JFontChooser.this.updateSampleFont();
        }
    }

    protected class ListSelectionHandler implements ListSelectionListener {
        private JTextComponent textComponent;

        ListSelectionHandler(JTextComponent textComponent) {
            this.textComponent = textComponent;
        }

        public void valueChanged(ListSelectionEvent e) {
            if(!e.getValueIsAdjusting()) {
                JList list = (JList)e.getSource();
                String selectedValue = (String)list.getSelectedValue();
                String oldValue = this.textComponent.getText();
                this.textComponent.setText(selectedValue);
                if(!oldValue.equalsIgnoreCase(selectedValue)) {
                    this.textComponent.selectAll();
                    this.textComponent.requestFocus();
                }

                JFontChooser.this.updateSampleFont();
            }

        }
    }
}
