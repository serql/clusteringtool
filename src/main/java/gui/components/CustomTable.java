package gui.components;


import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * @author Georgia Grigoriadou
 */

public class CustomTable extends JTable {
    private static final long serialVersionUID = 1L;
    private Class editingClass;
    private DefaultTableModel model;

    public CustomTable(){
        super();
        model = new DefaultTableModel();
        model.addColumn("");
        model.addColumn("");

        setModel(model);
        setBackground(Color.WHITE);
    }

    @Override
    public TableCellRenderer getCellRenderer(int row, int column) {
        editingClass = null;
        int modelColumn = convertColumnIndexToModel(column);
            Class rowClass = getModel().getValueAt(row, modelColumn).getClass();
            return getDefaultRenderer(rowClass);
    }

    @Override
    public TableCellEditor getCellEditor(int row, int column) {
        editingClass = null;
        int modelColumn = convertColumnIndexToModel(column);
            editingClass = getModel().getValueAt(row, modelColumn).getClass();
            return getDefaultEditor(editingClass);
    }

    @Override
    public Class getColumnClass(int column) {
        return editingClass != null ? editingClass : super.getColumnClass(column);
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        return col == 1;
    }

}