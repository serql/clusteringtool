package gui.components;

import javax.swing.border.Border;
import java.awt.*;

/**
 * @author Georgia Grigoriadou
 */

public class SubtleSquareBorder implements Border {
    private Color m_topColor = Color.lightGray;
    private Color m_bottomColor = Color.lightGray;
    private boolean roundC = false; // Do we want rounded corners on the border?

    public SubtleSquareBorder(boolean round_corners) {
        roundC = round_corners;
    }

    public Insets getBorderInsets(Component c) {
        int m_w = 6;
        int m_h = 6;
        return new Insets(m_h, m_w, m_h, m_w);
    }

    public boolean isBorderOpaque() {
        return true;
    }

    public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
        w = w - 3;
        h = h - 3;
        x++;
        y++;
// Rounded corners
        if (roundC) {
            g.setColor(m_topColor);
            g.drawLine(x, y + 2, x, y + h - 2);
            g.drawLine(x + 2, y, x + w - 2, y);
            g.drawLine(x, y + 2, x + 2, y); // Top left diagonal
            g.drawLine(x, y + h - 2, x + 2, y + h); // Bottom left diagonal
            g.setColor(m_bottomColor);
            g.drawLine(x + w, y + 2, x + w, y + h - 2);
            g.drawLine(x + 2, y + h, x + w - 2, y + h);
            g.drawLine(x + w - 2, y, x + w, y + 2); // Top right diagonal
            g.drawLine(x + w, y + h - 2, x + w - 2, y + h); // Bottom right diagonal
        }
// Square corners
        else {
            g.setColor(m_topColor);
            g.drawLine(x, y, x, y + h);
            g.drawLine(x, y, x + w, y);
            g.setColor(m_bottomColor);
            g.drawLine(x + w, y, x + w, y + h);
            g.drawLine(x, y + h, x + w, y + h);
        }
    }

}