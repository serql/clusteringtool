package gui.components;

import javax.swing.*;
import java.util.Map;
import java.util.function.Consumer;

/**
 * @author Georgia Grigoriadou
 */

public class DropDownButton extends JButton {
    private JPopupMenu popup;

    public DropDownButton() {
        super();
    }

	@SuppressWarnings("unchecked")
    public void setDropDownOptions(Map<String, Consumer> options) {
        popup = new JPopupMenu();

        for(Map.Entry<String, Consumer> option: options.entrySet()) {
            JMenuItem menuItem = new JMenuItem(option.getKey());
            menuItem.addActionListener(e ->
                    SwingUtilities.invokeLater(()-> option.getValue().accept(e))
            );

            popup.add(menuItem);
        }

        addActionListener(e -> SwingUtilities.invokeLater(
                () -> popup.show(this, 0, getHeight())
        ));
    }

	@SuppressWarnings("unchecked")
    public void setDropDownOptions(Map<String, Consumer> options, boolean checked) {
        popup = new JPopupMenu();

        for(Map.Entry<String, Consumer> option: options.entrySet()) {
            JCheckBoxMenuItem menuItem = new JCheckBoxMenuItem(option.getKey());
            menuItem.addActionListener(e ->
                    SwingUtilities.invokeLater(()-> option.getValue().accept(e))
            );
            menuItem.setState(checked);
            popup.add(menuItem);
        }

        addActionListener(e -> SwingUtilities.invokeLater(
                () -> popup.show(this, 0, getHeight())
        ));
    }

}
