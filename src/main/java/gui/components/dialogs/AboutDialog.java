package gui.components.dialogs;

import javax.swing.*;
import java.awt.*;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

public class AboutDialog extends JDialog {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/About");

    public AboutDialog() {
        super();
        initComponents();

        setTitle(bundle.getString("About.name"));
        setSize(450, 330);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);
        setModal(true);
        setVisible(true);
    }


    private void initComponents() {
        JLabel label         = new JLabel(new ImageIcon(bundle.getString("About.headerImagePath")));
        JEditorPane textArea = new JEditorPane("text/html", "");

        textArea.setText(bundle.getString("About.text"));
        setLayout(new BorderLayout());

        add(label, BorderLayout.NORTH);
        add(textArea, BorderLayout.CENTER);
    }

}
