package gui.components.dialogs;

import gui.components.CustomFileChooser;
import org.apache.commons.lang3.StringUtils;
import org.jfree.ui.tabbedui.VerticalLayout;

import javax.swing.*;
import javax.swing.plaf.basic.BasicBorders;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;

/**
 * @author Georgia Grigoriadou
 */

public class NewProjectDialog extends JDialog {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/NewProject");

    private JTabbedPane tabbedPane;
    private JPanel      filePanel;
    private JPanel      optionsPanel;
    private JPanel      nodesPanel;
    private JPanel      edgesPanel;
    private JPanel      buttonPanel;
    private JPanel      wEdgesPanel;
    private JPanel      populationPanel;
    private JTextField  edgesPathField;
    private JTextField  nodesPathField;
    private JButton     nodesButton;
    private JButton     edgesButton;
    private JButton     okButton;
    private JButton     cancelButton;
    private JCheckBox   weightEdges;
    private JTextField  population;
    private JLabel      clusteringOptions;
    private JLabel      wEdgesLabel;
    private JLabel      populationLabel;

    private String  nodesPath;
    private String  edgesPath;
    private boolean okPressed;

    public NewProjectDialog() {
        super();
        initComponents();

        setTitle(bundle.getString("NewProject.name"));
        setSize(300, 220);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);
        setModal(true);
        setVisible(true);
    }

    private void initComponents() {
        tabbedPane        = new JTabbedPane();
        filePanel         = new JPanel();
        optionsPanel      = new JPanel();
        nodesPanel        = new JPanel();
        edgesPanel        = new JPanel();
        buttonPanel       = new JPanel();
        wEdgesPanel       = new JPanel();
        populationPanel   = new JPanel();
        nodesPathField    = new JTextField();
        edgesPathField    = new JTextField();
        nodesButton       = new JButton();
        edgesButton       = new JButton();
        okButton          = new JButton();
        cancelButton      = new JButton();
        population        = new JTextField();
        weightEdges       = new JCheckBox();
        clusteringOptions = new JLabel();
        wEdgesLabel       = new JLabel();
        populationLabel   = new JLabel();

        filePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 10));
        optionsPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 10));

        nodesButton.setIcon(new ImageIcon(bundle.getString("NewProject.openFileIconPath")));
        edgesButton.setIcon(new ImageIcon(bundle.getString("NewProject.openFileIconPath")));

        nodesPathField.setText(bundle.getString("NewProject.chooseNodeFile"));
        nodesPathField.setPreferredSize(new Dimension(250, 24));
        nodesPathField.setRequestFocusEnabled(false);

        edgesPathField.setText(bundle.getString("NewProject.chooseEdgeFile"));
        edgesPathField.setPreferredSize(new Dimension(250, 24));
        edgesPathField.setRequestFocusEnabled(false);

        okButton.setText(bundle.getString("NewProject.ok"));
        cancelButton.setText(bundle.getString("NewProject.cancel"));

        clusteringOptions.setText(bundle.getString("NewProject.clusteringOptions"));

        wEdgesLabel.setText(bundle.getString("NewProject.weightEdges"));
        populationLabel.setText(bundle.getString("NewProject.initialPopulation"));

        population.setText(bundle.getString("NewProject.auto"));
        population.setInputVerifier(new TextFieldVerifier());
        Dimension dimension = new Dimension(40, 20);

        population.setMinimumSize(dimension);
        population.setPreferredSize(dimension);
        population.setMaximumSize(dimension);

        weightEdges.setSelected(true);
        okButton.setEnabled(false);

        addComponents();
        addListeners();
    }

    private void addComponents(){
        setLayout(new BorderLayout());
        filePanel.setLayout(new VerticalLayout(true));
        nodesPanel.setLayout(new BorderLayout(5,5));
        edgesPanel.setLayout(new BorderLayout(5,5));
        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER,5,5));
        optionsPanel.setLayout(new VerticalLayout(true));
        wEdgesPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        populationPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        nodesPanel.add(nodesButton, BorderLayout.WEST);
        nodesPanel.add(nodesPathField, BorderLayout.CENTER);

        edgesPanel.add(edgesButton, BorderLayout.WEST);
        edgesPanel.add(edgesPathField, BorderLayout.CENTER);

        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        wEdgesPanel.add(wEdgesLabel);
        wEdgesPanel.add(weightEdges);

        populationPanel.add(populationLabel);
        populationPanel.add(population);

        filePanel.add(nodesPanel);
        filePanel.add(new JPanel());
        filePanel.add(edgesPanel);

        optionsPanel.add(clusteringOptions);
        optionsPanel.add(new JPanel());
        optionsPanel.add(populationPanel);
        optionsPanel.add(new JPanel());
        optionsPanel.add(wEdgesPanel);

        tabbedPane.add(bundle.getString("NewProject.file"), filePanel);
        tabbedPane.add(bundle.getString("NewProject.options"), optionsPanel);

        add(tabbedPane, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    private void addListeners(){
        nodesButton.addActionListener(this::chooseFile);
        edgesButton.addActionListener(this::chooseFile);
        okButton.addActionListener(
                e -> {
                    if (population.getInputVerifier().verify(population)) {
                        okPressed = true;
                        dispose();
                    }
                });
        cancelButton.addActionListener(e -> dispose());
    }

    private void chooseFile(ActionEvent event){
        CustomFileChooser file = new CustomFileChooser();
        String path            = file.showOpenCSVDialog();
        if (path != null) {
            if(event.getSource() == nodesButton) {
                nodesPath = path;
                nodesPathField.setText(path);
            } else {
                edgesPath = path;
                edgesPathField.setText(path);
            }
        }
        okButton.setEnabled(edgesPath != null && nodesPath != null);
    }

    public String[] getPaths(){
        String[] paths = new String[2];
        if(okPressed) {
            paths[0] = nodesPath;
            paths[1] = edgesPath;
        }
        return paths;
    }

    public boolean weightEdges(){
        return weightEdges.isSelected();
    }

    public int getInitialPopulation(){
        int initialPopulation = 0;
        if(StringUtils.isNumeric(population.getText())){
            initialPopulation = Integer.parseInt(population.getText());
        }
        return initialPopulation;
    }

    private class TextFieldVerifier extends InputVerifier {
        JTextField textField = new JTextField();

        @Override
        public boolean verify(JComponent input) {
            String text = ((JTextField) input).getText();
            return StringUtils.isNumeric(text) || text.equals(bundle.getString("NewProject.auto"));
        }

        @Override
        public boolean shouldYieldFocus(JComponent input) {
            if (!verify(input)) {
                input.setBorder(new BasicBorders.FieldBorder(Color.RED, Color.RED, Color.RED, Color.RED));
                JOptionPane.showMessageDialog(null, bundle.getString("NewProject.nonLegalValue") + ((JTextField) input).getText());
            } else {
                input.setBorder(textField.getBorder());
            }
            return verify(input);
        }
    }
}

