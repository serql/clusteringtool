package gui.appearance;

import controller.visualization.Appearance;
import gui.components.CustomColorPicker;
import gui.resources.ResourceBundleList;
import org.jfree.ui.tabbedui.VerticalLayout;

import javax.swing.*;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;
import java.util.ResourceBundle;

/**
 * @author Georgia Grigoriadou
 */

class LabelColorPanel extends AppearancePanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Appearance");

    private JPanel            uniqueOptionPanel;
    private JPanel            rankingOptionPanel;
    private JPanel            rankingColorPanel;
    private JRadioButton      uniqueRadioButton;
    private JRadioButton      rankingRadioButton;
    private JLabel            colorLabel;
    private JComboBox<String> degreeComboBox;
    private JButton           colorButton;
    private JButton           rankingColorButton1;
    private JButton           rankingColorButton2;

    private Color uniqueLabelColor   = Color.BLACK;
    private Color rankingLabelColor1 = Color.WHITE;
    private Color rankingLabelColor2 = Color.BLACK;

    LabelColorPanel() {
        super();
        initComponents();
    }

    private void initComponents() {
        uniqueOptionPanel   = new JPanel();
        rankingOptionPanel  = new JPanel();
        rankingColorPanel   = new JPanel();
        uniqueRadioButton   = new JRadioButton();
        rankingRadioButton  = new JRadioButton();
        colorLabel          = new JLabel();
        degreeComboBox      = new JComboBox<>();
        colorButton         = new JButton();
        rankingColorButton1 = new JButton();
        rankingColorButton2 = new JButton();

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(uniqueRadioButton);
        buttonGroup.add(rankingRadioButton);

        uniqueRadioButton.setText(bundle.getString("Appearance.unique"));
        rankingRadioButton.setText(bundle.getString("Appearance.ranking"));
        colorLabel.setText("#ffffff");

        colorButton.setBackground(Color.BLACK);
        rankingColorButton1.setBackground(Color.WHITE);
        rankingColorButton2.setBackground(Color.BLACK);

        colorButton.setUI(new BasicButtonUI());
        rankingColorButton1.setUI(new BasicButtonUI());
        rankingColorButton2.setUI(new BasicButtonUI());

        colorButton.setPreferredSize(new Dimension(16, 16));
        rankingColorButton1.setPreferredSize(new Dimension(16, 16));
        rankingColorButton2.setPreferredSize(new Dimension(16, 16));


        for (String entry : ResourceBundleList.getPropertyStringArray(bundle, "Appearance.degreeOptions")) {
            degreeComboBox.addItem(entry);
        }

        addComponents();
        addListeners();

        uniqueOptionPanel.setVisible(false);
        rankingOptionPanel.setVisible(false);
    }

    private void addComponents() {
        uniqueOptionPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        rankingOptionPanel.setLayout(new VerticalLayout());
        rankingColorPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        panel.add(uniqueRadioButton);
        uniqueOptionPanel.add(colorButton);
        uniqueOptionPanel.add(colorLabel);
        panel.add(uniqueOptionPanel);
        panel.add(rankingRadioButton);
        rankingOptionPanel.add(degreeComboBox);
        rankingColorPanel.add(rankingColorButton1);
        rankingColorPanel.add(rankingColorButton2);
        rankingOptionPanel.add(rankingColorPanel);
        panel.add(rankingOptionPanel);
    }

    private void addListeners() {
        uniqueRadioButton.addActionListener(e -> {
            if (enableAutoTransformation) {
                Appearance.setUniqueLabelColor(uniqueLabelColor);
            }
            uniqueOptionPanel.setVisible(true);
            rankingOptionPanel.setVisible(false);
        });

        rankingRadioButton.addActionListener(e -> {
            uniqueOptionPanel.setVisible(false);
            rankingOptionPanel.setVisible(true);
            if (enableAutoTransformation) {
                Appearance.setRankingLabelColor(String.valueOf(degreeComboBox.getSelectedItem()), rankingLabelColor1, rankingLabelColor2);
            }
        });
        colorButton.addActionListener(e -> new CustomColorPicker(true, false,
                color -> {
                    if (enableAutoTransformation) {
                        Appearance.setUniqueLabelColor(color);
                    }
                    colorButton.setBackground(color);
                    colorLabel.setText("#" + Integer.toHexString(color.getRGB()).substring(2));
                    uniqueLabelColor = color;
                }));
        degreeComboBox.addActionListener(e -> Appearance.setRankingLabelColor(String.valueOf(degreeComboBox.getSelectedItem()), rankingLabelColor1, rankingLabelColor2));
        rankingColorButton1.addActionListener(e -> new CustomColorPicker(true, false,
                color -> {
                    if (enableAutoTransformation) {
                        Appearance.setRankingLabelColor(String.valueOf(degreeComboBox.getSelectedItem()), color, rankingLabelColor2);
                    }
                    rankingColorButton1.setBackground(color);
                    rankingLabelColor1 = color;
                }));
        rankingColorButton2.addActionListener(e -> new CustomColorPicker(true, false,
                color -> {
                    if (enableAutoTransformation) {
                        Appearance.setRankingLabelColor(String.valueOf(degreeComboBox.getSelectedItem()), rankingLabelColor1, color);
                    }
                    rankingColorButton2.setBackground(color);
                    rankingLabelColor2 = color;
                }));
    }

    @Override
    void apply() {
        if (uniqueRadioButton.isSelected()) {
            Appearance.setUniqueLabelColor(uniqueLabelColor);
        }
        if (rankingRadioButton.isSelected()) {
            Appearance.setRankingLabelColor(String.valueOf(degreeComboBox.getSelectedItem()), rankingLabelColor1, rankingLabelColor2);
        }
    }
}
