package gui.resources;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Gewrgia
 */

@XmlRootElement(name="History")
@XmlAccessorType(XmlAccessType.FIELD)
public class History {

    @XmlElement(name = "recentFile")
    private static List<FileProperties> recentFiles = new LinkedList<>();

    public static void writeHistoryFile() {
        try {
            JAXBContext jaxbContext   = JAXBContext.newInstance(FileProperties.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
            FileOutputStream fileOutputStream = new FileOutputStream("src/main/resources/history.xml");
            OutputStreamWriter writer = new OutputStreamWriter(fileOutputStream);
            writer.write("<?xml version='1.0' encoding='UTF-8' standalone='yes'?>");
            writer.write("\n<History>\n");
            for (FileProperties recentFile : recentFiles) {
                jaxbMarshaller.marshal(recentFile, writer);
                writer.write("\n");
            }
            writer.write("</History>");
            writer.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void readHistoryFile() {
        List<FileProperties> list = new LinkedList<>();
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(History.class);
            Unmarshaller jaxbUnmarshaller;
            jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            History history;
            history = (History) jaxbUnmarshaller.unmarshal(new File("src/main/resources/history.xml"));
            list.addAll(history.getRecentFiles().stream()
                    .map(fileProperties -> new FileProperties(fileProperties.getName(), fileProperties.getLocation(), fileProperties.getType()))
                    .collect(Collectors.toList()));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        list.forEach(History::addRecentFile);
    }

    public static void addRecentFile(FileProperties recentFile) {
        for (FileProperties file : recentFiles) {
            if (file.getLocation().equals(recentFile.getLocation())) {
                recentFiles.remove(file);
                break;
            }
        }
        recentFiles.add(recentFile);
    }

    private List<FileProperties> getRecentFiles() {
        return recentFiles;
    }

    public static List<FileProperties> getMostRecentFiles(int number) {
        List<FileProperties> files = new LinkedList<>(recentFiles);
        Collections.reverse(files);
        List<FileProperties> mostRecentFiles = new LinkedList<>();

        int i = 0;
        Iterator<FileProperties> iterator = files.iterator();
        while (iterator.hasNext() && i < number) {
            FileProperties recentFile = iterator.next();
            File file = new File(recentFile.getLocation());
            if(file.exists()) {
                mostRecentFiles.add(recentFile);
                i++;
            } else {
                recentFiles.remove(recentFile);
            }
        }
        return mostRecentFiles;
    }

}
