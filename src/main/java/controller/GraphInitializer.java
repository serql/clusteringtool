package controller;

import com.opencsv.CSVReader;
import model.api.Edge;
import model.api.Graph;
import model.api.Node;
import model.TriFunction;
import model.impl.EdgeImpl;
import model.impl.GraphImpl;
import model.impl.NodeImpl;
import org.gephi.graph.api.DirectedGraph;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author Georgia Grigoriadou
 */

public class GraphInitializer {

    private static Map<Integer, Node> nodesMap;

    public GraphInitializer() {
        nodesMap = new HashMap<>();
    }

    @SuppressWarnings("unchecked")
    public TriFunction<String, String,Boolean, Graph> initialize = (nodesPath, edgesPath, weightedEdges) -> {
        nodesMap = new ConcurrentHashMap<>();
        Graph graph = new GraphImpl();
        graph.setNodes((Set<Node>) readCSVFile.apply(nodesPath, createNode));
        graph.setEdges((Set<Edge>) readCSVFile.apply(edgesPath, createEdge));
        graph = addNeighbours.apply(graph, weightedEdges);
        return graph;
    };

    Function<DirectedGraph, Graph> createGraph = (directedGraph) -> {
        Graph graph     = new GraphImpl();
        Set<Node> nodes = new HashSet<>();
        Set<Edge> edges = new HashSet<>();

        directedGraph.getNodes().forEach(n -> {
            Node node = new NodeImpl((int) Float.parseFloat((String) n.getId()), n.getLabel());
            nodes.add(node);
            nodesMap.put(node.getId(), node);
        });
        directedGraph.getEdges().forEach(e -> {
            Edge edge = new EdgeImpl((int) Float.parseFloat((String)e.getSource().getId()),(int) Float.parseFloat((String) e.getTarget().getId()), "Directed", (float) e.getWeight());
            edges.add(edge);
        });

        graph.setNodes(nodes);
        graph.setEdges(edges);
        graph = addNeighbours.apply(graph, true);
        return graph;
    };

    private static BiFunction<String, Function<String[], Object>, Object> readCSVFile = (path, function) -> {
        CSVReader reader    = null;
        Set<Object> objects = new HashSet<>();

        try {
            reader = new CSVReader(new FileReader(path));
            String[] line;

            while ((line = reader.readNext()) != null) {
                Optional<Object> object = Optional.ofNullable(function.apply(line));
                if (object.isPresent()) { objects.add(object.get()); }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return objects;
    };

    private static Function<String[], Object> createNode = (line) -> {
        Node node = null;
        try {
            int id = Integer.parseInt(line[0]);
            if(!nodesMap.containsKey(id)) {
                node = new NodeImpl(id, line[1]);
                nodesMap.put(id, node);
            }
        } catch (NumberFormatException | NullPointerException ignored) {
        }

        return node;
    };

    private static Function<String[], Object> createEdge = (line) -> {
        Edge edge = null;
        try {
            int source   = Integer.parseInt(line[0]);
            int target   = Integer.parseInt(line[1]);
            float weight = Float.parseFloat(line[3]);

            edge = new EdgeImpl(source, target, line[2], weight);
        } catch (NumberFormatException | NullPointerException ignored) {
        }

        return edge;
    };

    private static BiFunction<Graph,Boolean, Graph> addNeighbours = (graph, weightedEdges) -> {
        Set<Edge> removedEdges = new HashSet<>();
        graph.getEdges().stream()
                .filter(edge -> !nodesMap.containsKey(edge.getSourceID()) || !nodesMap.containsKey(edge.getTargetID()))
                .forEach(removedEdges::add);
        removedEdges.forEach(
                graph.getEdges()::remove);

        graph.getEdges().stream()
                .filter(edge -> nodesMap.containsKey(edge.getSourceID()) && nodesMap.containsKey(edge.getTargetID()))
                .forEach(edge -> {
                    Node sourceNode = nodesMap.get(edge.getSourceID());
                    Node targetNode = nodesMap.get(edge.getTargetID());

                    if (!sourceNode.getNeighbours().containsKey(targetNode)) {
                        if(weightedEdges) {
                            sourceNode.addNeighbour(targetNode, edge.getWeight());
                        } else {
                            sourceNode.addNeighbour(targetNode,(float) 1);
                        }
                    }
                    if (!targetNode.getNeighbours().containsKey(sourceNode)) {
                        if(weightedEdges) {
                            targetNode.addNeighbour(sourceNode, edge.getWeight());
                        } else {
                            targetNode.addNeighbour(sourceNode,(float) 1);

                        }
                    }
                });

        return graph;
    };
}
