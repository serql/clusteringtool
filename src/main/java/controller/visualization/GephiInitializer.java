package controller.visualization;

import controller.Project;
import model.api.*;
import org.gephi.appearance.api.*;
import org.gephi.appearance.plugin.PartitionElementColorTransformer;
import org.gephi.appearance.plugin.palette.Palette;
import org.gephi.appearance.plugin.palette.PaletteManager;
import org.gephi.graph.api.Column;
import org.gephi.graph.api.DirectedGraph;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.io.importer.api.EdgeDirectionDefault;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.layout.plugin.force.StepDisplacement;
import org.gephi.layout.plugin.force.yifanHu.YifanHu;
import org.gephi.layout.plugin.force.yifanHu.YifanHuLayout;
import org.gephi.layout.plugin.noverlap.NoverlapLayout;
import org.gephi.layout.plugin.noverlap.NoverlapLayoutBuilder;
import org.gephi.layout.plugin.random.RandomLayout;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.gephi.project.io.LoadTask;
import org.openide.util.Lookup;

import java.awt.*;
import java.io.File;

/**
 * @author Georgia Grigoriadou
 */

@SuppressWarnings("WeakerAccess")
public class GephiInitializer {
    private static ProjectController pc;
    private static Workspace         workspace;
    private static GraphModel        graphModel;
    private static DirectedGraph     directedGraph;

    public static final String DIRECTED    = "Directed";
    public static final String INTRA_EDGE  = "IntraEdge";
    public static final String INTER_EDGE  = "InterEdge";
    public static final String CLUSTER     = "Cluster";
    public static final String INDEPENDENT = "Independent";

    public static Workspace newWorkspace() {
        pc = Lookup.getDefault().lookup(ProjectController.class);
        pc.newProject();

        return workspace = pc.getCurrentWorkspace();
    }

    public static GraphModel createDirectedGraph(Graph graph, MDG mdg) {
        graphModel    = Lookup.getDefault().lookup(GraphController.class).getGraphModel(workspace);
        directedGraph = graphModel.getDirectedGraph();
        graphModel.getNodeTable().addColumn(CLUSTER, String.class);
        graphModel.getEdgeTable().addColumn(INTRA_EDGE, String.class);
        graphModel.getEdgeTable().addColumn(INTER_EDGE, String.class);

        for (Cluster cl : mdg.getClusters()) {
            for (Node node : cl.getNodes()) {
                org.gephi.graph.api.Node gephiNode = graphModel.factory().newNode(String.valueOf(node.getId()));

                gephiNode.setLabel(node.getName());
                gephiNode.setColor(Color.orange);
                gephiNode.setSize(5);
                gephiNode.setAttribute(CLUSTER, cl.getId());
                directedGraph.addNode(gephiNode);
                gephiNode.getTextProperties().setText(node.getName());
                gephiNode.getTextProperties().setVisible(true);
            }
        }
        for (Node node : mdg.getIndependentNodes()) {
            org.gephi.graph.api.Node gephiNode = graphModel.factory().newNode(String.valueOf(node.getId()));

            gephiNode.setLabel(node.getName());
            gephiNode.setColor(Color.gray);
            gephiNode.setSize(5);
            gephiNode.setAttribute(CLUSTER, "Independent");
            directedGraph.addNode(gephiNode);
            gephiNode.getTextProperties().setText(node.getName());
            gephiNode.getTextProperties().setVisible(true);
        }

        for (Edge edge : graph.getEdges()) {
            boolean isDirected = false;
            org.gephi.graph.api.Node source = directedGraph.getNode(String.valueOf(edge.getSourceID()));
            org.gephi.graph.api.Node target = directedGraph.getNode(String.valueOf(edge.getTargetID()));

            if (edge.getType().equals(DIRECTED)) {
                isDirected = true;
            }
            boolean intra = source.getAttribute(CLUSTER).equals(target.getAttribute(CLUSTER));

            org.gephi.graph.api.Edge gephiEdge = graphModel.factory().newEdge(
                    directedGraph.getNode(Integer.toString(edge.getSourceID())),
                    directedGraph.getNode(Integer.toString(edge.getTargetID())),
                    0,
                    edge.getWeight(),
                    isDirected
            );
            gephiEdge.setAttribute(INTRA_EDGE, String.valueOf(intra));
            gephiEdge.setAttribute(INTER_EDGE, String.valueOf(!intra));
            directedGraph.addEdge(gephiEdge);
        }

        AppearanceController appearanceController = Lookup.getDefault().lookup(AppearanceController.class);
        AppearanceModel appearanceModel           = appearanceController.getModel();
        Column column       = graphModel.getNodeTable().getColumn(GephiInitializer.CLUSTER);
        Function func       = appearanceModel.getNodeFunction(directedGraph, column, PartitionElementColorTransformer.class);
        Partition partition = ((PartitionFunction) func).getPartition();
        Palette palette     = PaletteManager.getInstance().generatePalette(partition.size());
        partition.setColors(palette.getColors());
        appearanceController.transform(func);

        RandomLayout randomLayout = new RandomLayout(new org.gephi.layout.plugin.random.Random(), 1500);
        randomLayout.setGraphModel(graphModel);
        Layout.startLayout(randomLayout);
        YifanHuLayout yifanHuLayout = new YifanHuLayout(new YifanHu(), new StepDisplacement(1f));
        yifanHuLayout.setGraphModel(graphModel);
        yifanHuLayout.resetPropertiesValues();
        Layout.startLayout(yifanHuLayout);
        NoverlapLayout noverlapLayout = new NoverlapLayout(new NoverlapLayoutBuilder());
        noverlapLayout.resetPropertiesValues();
        noverlapLayout.setGraphModel(graphModel);
        Layout.startLayout(noverlapLayout);

        return graphModel;
    }

    public static GraphModel openGraph(String path){
        ImportController importController = Lookup.getDefault().lookup(ImportController.class);
        graphModel                        = Lookup.getDefault().lookup(GraphController.class).getGraphModel();

        org.gephi.io.importer.api.Container container;
        try {
            File file = new File(path);
            container = importController.importFile(file);
            container.getLoader().setEdgeDefault(EdgeDirectionDefault.DIRECTED);   //Force DIRECTED
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

        importController.process(container, new DefaultProcessor(), workspace);
        directedGraph = graphModel.getDirectedGraph();

        for (org.gephi.graph.api.Node node : directedGraph.getNodes()) {

            node.getTextProperties().setText(node.getLabel());
            node.getTextProperties().setVisible(true);
        }

        if(graphModel.getNodeTable().getColumn(CLUSTER) == null) {
            Project.initializeGraph(directedGraph);
            Project.cluster(0);
            Project.setGephi();
            graphModel = Project.getGraphModel();
        }

        return graphModel;
    }
    public static GraphModel openGephi(String path){
        LoadTask task = new LoadTask(new File(path));
        task.run();

        graphModel    = Lookup.getDefault().lookup(GraphController.class).getGraphModel();
        directedGraph = graphModel.getDirectedGraph();

        for (org.gephi.graph.api.Node node : directedGraph.getNodes()) {

            node.getTextProperties().setText(node.getLabel());
            node.getTextProperties().setVisible(true);
        }

        return graphModel;
    }

    public static void reset(){
        pc            = null;
        workspace     = null;
        graphModel    = null;
        directedGraph = null;
    }

}
