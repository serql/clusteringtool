package controller.visualization;

import org.gephi.appearance.api.*;
import org.gephi.appearance.plugin.*;
import org.gephi.appearance.plugin.palette.Palette;
import org.gephi.appearance.plugin.palette.PaletteManager;
import org.gephi.appearance.plugin.palette.Preset;
import org.gephi.graph.api.Column;
import org.gephi.graph.api.DirectedGraph;
import org.gephi.graph.api.Node;
import org.gephi.preview.api.PreviewController;
import org.gephi.preview.api.PreviewModel;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.preview.types.DependantOriginalColor;
import org.gephi.preview.types.EdgeColor;
import org.openide.util.Lookup;
import controller.Project;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import static controller.visualization.Preview.*;

/**
 * @author Georgia Grigoriadou
 */

public class Appearance {
    private static Map<String, Palette> palettes = new HashMap<>();

    private static AppearanceController appearanceController;
    private static AppearanceModel      appearanceModel;

    static final String DEGREE     = "Degree";
    static final String IN_DEGREE  = "In-Degree";
    static final String OUT_DEGREE = "Out-Degree";

    static boolean nSizeDegree     = false;
    static boolean nSizeInDegree   = false;
    static boolean nSizeOutDegree  = false;
    static boolean nColorDegree    = false;
    static boolean nColorInDegree  = false;
    static boolean nColorOutDegree = false;
    static boolean lSizeDegree     = false;
    static boolean lSizeInDegree   = false;
    static boolean lSizeOutDegree  = false;
    static boolean lColorDegree    = false;
    static boolean lColorInDegree  = false;
    static boolean lColorOutDegree = false;

    static Color nColor         = null;
    static Color nRankingColor1 = null;
    static Color nRankingColor2 = null;
    static Color lColor         = null;
    static Color lRankingColor1 = null;
    static Color lRankingColor2 = null;
    static Color eColor         = null;
    static Color eRankingColor1 = null;
    static Color eRankingColor2 = null;
    static Color intraColor     = null;
    static Color interColor     = null;

    public static Partition partition;
    public static double nodeMinSize  = Double.MAX_VALUE;
    public static double nodeMaxSize  = Double.MIN_VALUE;
    public static double labelMinSize = Double.MAX_VALUE;
    public static double labelMaxSize = Double.MIN_VALUE;

    public static void setUniqueNodeColor(Color color) {
        Function[] functions = appearanceModel.getNodeFunctions(Project.getGraphModel().getDirectedGraph());
        Function function    = functions[3];

        UniqueElementColorTransformer transformer = function.getTransformer();
        transformer.setColor(color);
        appearanceController.transform(function);

        Preview.refreshPreview();

        nColorDegree    = false;
        nColorInDegree  = false;
        nColorOutDegree = false;
        nColor          = color;
        nRankingColor1  = null;
        nRankingColor2  = null;
    }

    public static void setClusterNodeColor(String paletteName) {
        Column column       = Project.getGraphModel().getNodeTable().getColumn(GephiInitializer.CLUSTER);
        Function func       = appearanceModel.getNodeFunction(Project.getGraphModel().getDirectedGraph(), column, PartitionElementColorTransformer.class);
        Partition partition = ((PartitionFunction) func).getPartition();
        Palette palette     = getPalette(paletteName, partition.size());
        partition.setColors(palette.getColors());
        appearanceController.transform(func);

        refreshPreview();

        nColorDegree    = false;
        nColorInDegree  = false;
        nColorOutDegree = false;
        nColor          = null;
        nRankingColor1  = null;
        nRankingColor2  = null;
    }

    public static void setRankingNodeColor(String degree, Color color1, Color color2) {
        Function function;
        switch (degree) {
            case OUT_DEGREE:
                function = appearanceModel.getNodeFunction(Project.getGraphModel().getDirectedGraph(), AppearanceModel.GraphFunction.NODE_OUTDEGREE, RankingElementColorTransformer.class);
                break;
            case IN_DEGREE:
                function = appearanceModel.getNodeFunction(Project.getGraphModel().getDirectedGraph(), AppearanceModel.GraphFunction.NODE_INDEGREE, RankingElementColorTransformer.class);
                break;
            default:
                function = appearanceModel.getNodeFunction(Project.getGraphModel().getDirectedGraph(), AppearanceModel.GraphFunction.NODE_DEGREE, RankingElementColorTransformer.class);
        }
        RankingElementColorTransformer degreeTransformer = function.getTransformer();
        degreeTransformer.setColors(new Color[]{color1, color2});
        degreeTransformer.setColorPositions(new float[]{0f, 1f});
        appearanceController.transform(function);

        refreshPreview();

        nColorDegree    = degree.equals(DEGREE);
        nColorInDegree  = degree.equals(IN_DEGREE);
        nColorOutDegree = degree.equals(OUT_DEGREE);
        nColor          = null;
        nRankingColor1  = color1;
        nRankingColor2  = color2;
    }

    public static void setUniqueNodeSize(double size) {
        Function[] functions = appearanceModel.getNodeFunctions(Project.getGraphModel().getDirectedGraph());
        Function function    = functions[0];

        UniqueNodeSizeTransformer transformer = function.getTransformer();
        transformer.setSize((float) size);
        appearanceController.transform(function);

        refreshPreview();

        nSizeDegree    = false;
        nSizeInDegree  = false;
        nSizeOutDegree = false;
        nodeMinSize    = size;
        nodeMaxSize    = size;
    }

    public static void setRankNodeSize(String degree, double min, double max) {
        Function function;
        switch (degree) {
            case OUT_DEGREE:
                function = appearanceModel.getNodeFunction(Project.getGraphModel().getDirectedGraph(), AppearanceModel.GraphFunction.NODE_OUTDEGREE, RankingNodeSizeTransformer.class);
                break;
            case IN_DEGREE:
                function = appearanceModel.getNodeFunction(Project.getGraphModel().getDirectedGraph(), AppearanceModel.GraphFunction.NODE_INDEGREE, RankingNodeSizeTransformer.class);
                break;
            default:
                function = appearanceModel.getNodeFunction(Project.getGraphModel().getDirectedGraph(), AppearanceModel.GraphFunction.NODE_DEGREE, RankingNodeSizeTransformer.class);
        }
        RankingNodeSizeTransformer transformer = function.getTransformer();
        transformer.setMinSize((float) min);
        transformer.setMaxSize((float) max);
        appearanceController.transform(function);

        refreshPreview();

        nSizeDegree    = degree.equals(DEGREE);
        nSizeInDegree  = degree.equals(IN_DEGREE);
        nSizeOutDegree = degree.equals(OUT_DEGREE);
        nodeMinSize    = min;
        nodeMaxSize    = max;
    }

    public static void setUniqueLabelColor(Color color) {

        Function[] functions = appearanceModel.getNodeFunctions(Project.getGraphModel().getDirectedGraph());
        Function function    = functions[1];
        PreviewController previewController = Lookup.getDefault().lookup(PreviewController.class);
        PreviewModel previewModel           = previewController.getModel();
        previewModel.getProperties().putValue(PreviewProperty.NODE_LABEL_COLOR, new DependantOriginalColor(DependantOriginalColor.Mode.ORIGINAL));

        UniqueLabelColorTransformer transformer = function.getTransformer();
        transformer.setColor(color);
        appearanceController.transform(function);

        refreshPreview();


        lColorDegree    = false;
        lColorInDegree  = false;
        lColorOutDegree = false;
        lColor          = color;
        lRankingColor1  = null;
        lRankingColor2  = null;
    }

    public static void setRankingLabelColor(String degree, Color color1, Color color2) {
        Function function;
        switch (degree) {
            case OUT_DEGREE:
                function = appearanceModel.getNodeFunction(Project.getGraphModel().getDirectedGraph(), AppearanceModel.GraphFunction.NODE_OUTDEGREE, RankingLabelColorTransformer.class);
                break;
            case IN_DEGREE:
                function = appearanceModel.getNodeFunction(Project.getGraphModel().getDirectedGraph(), AppearanceModel.GraphFunction.NODE_INDEGREE, RankingLabelColorTransformer.class);
                break;
            default:
                function = appearanceModel.getNodeFunction(Project.getGraphModel().getDirectedGraph(), AppearanceModel.GraphFunction.NODE_DEGREE, RankingLabelColorTransformer.class);
        }

        PreviewController previewController = Lookup.getDefault().lookup(PreviewController.class);
        PreviewModel previewModel           = previewController.getModel();
        previewModel.getProperties().putValue(PreviewProperty.NODE_LABEL_COLOR, new DependantOriginalColor(DependantOriginalColor.Mode.ORIGINAL));

        RankingLabelColorTransformer degreeTransformer = function.getTransformer();
        degreeTransformer.setColors(new Color[]{color1, color2});
        degreeTransformer.setColorPositions(new float[]{0f, 1f});
        appearanceController.transform(function);

        refreshPreview();

        lColorDegree    = degree.equals(DEGREE);
        lColorInDegree  = degree.equals(IN_DEGREE);
        lColorOutDegree = degree.equals(OUT_DEGREE);
        lColor          = null;
        lRankingColor1  = color1;
        lRankingColor2  = color2;
    }

    public static void setUniqueLabelSize(double size) {
        Function[] functions = appearanceModel.getNodeFunctions(Project.getGraphModel().getDirectedGraph());
        Function function    = functions[2];

        UniqueLabelSizeTransformer transformer = function.getTransformer();
        transformer.setSize((float) size);
        appearanceController.transform(function);

        refreshPreview();

        labelSize = (float) size;

        lSizeDegree    = false;
        lSizeInDegree  = false;
        lSizeOutDegree = false;
        labelMinSize   = size;
        labelMaxSize   = size;
    }

    public static void setRankLabelSize(String degree, double min, double max) {

        Function function;
        switch (degree) {
            case OUT_DEGREE:
                function = appearanceModel.getNodeFunction(Project.getGraphModel().getDirectedGraph(), AppearanceModel.GraphFunction.NODE_OUTDEGREE, RankingLabelSizeTransformer.class);
                break;
            case IN_DEGREE:
                function = appearanceModel.getNodeFunction(Project.getGraphModel().getDirectedGraph(), AppearanceModel.GraphFunction.NODE_INDEGREE, RankingLabelSizeTransformer.class);
                break;
            default:
                function = appearanceModel.getNodeFunction(Project.getGraphModel().getDirectedGraph(), AppearanceModel.GraphFunction.NODE_DEGREE, RankingLabelSizeTransformer.class);
        }
        RankingLabelSizeTransformer transformer = function.getTransformer();
        transformer.setMinSize((float) min);
        transformer.setMaxSize((float) max);
        appearanceController.transform(function);

        refreshPreview();

        lSizeDegree    = degree.equals(DEGREE);
        lSizeInDegree  = degree.equals(IN_DEGREE);
        lSizeOutDegree = degree.equals(OUT_DEGREE);
        labelMinSize   = min;
        labelMaxSize   = max;
    }

    public static void setUniqueEdgeColor(Color color) {
        Function[] functions = appearanceModel.getEdgeFunctions(Project.getGraphModel().getDirectedGraph());
        Function function    = functions[0];
        PreviewController previewController = Lookup.getDefault().lookup(PreviewController.class);
        previewController.getModel().getProperties().putValue(PreviewProperty.EDGE_COLOR, new EdgeColor(EdgeColor.Mode.ORIGINAL));

        UniqueElementColorTransformer transformer = function.getTransformer();
        transformer.setColor(color);
        appearanceController.transform(function);

        refreshPreview();

        eColor         = color;
        eRankingColor1 = null;
        eRankingColor2 = null;
        intraColor     = null;
        interColor     = null;
    }

    public static void setInterIntraEdgeColor(Color color1, Color color2) {
        Column column = Project.getGraphModel().getEdgeTable().getColumn(GephiInitializer.INTRA_EDGE);
        PreviewController previewController = Lookup.getDefault().lookup(PreviewController.class);
        previewController.getModel().getProperties().putValue(PreviewProperty.EDGE_COLOR, new EdgeColor(EdgeColor.Mode.ORIGINAL));
        Function function   = appearanceModel.getEdgeFunction(Project.getGraphModel().getDirectedGraph(), column, PartitionElementColorTransformer.class);
        Partition partition = ((PartitionFunction) function).getPartition();
        Color[] colors      = {color1, color2};
        partition.setColors(colors);
        appearanceController.transform(function);

        refreshPreview();

        eColor         = null;
        eRankingColor1 = null;
        eRankingColor2 = null;
        intraColor     = color2;
        interColor     = color1;
    }

    public static void setRankingEdgeColor(Color color1, Color color2) {
        Function function = appearanceModel.getEdgeFunction(Project.getGraphModel().getDirectedGraph(), AppearanceModel.GraphFunction.EDGE_WEIGHT, RankingElementColorTransformer.class);

        PreviewController previewController = Lookup.getDefault().lookup(PreviewController.class);
        previewController.getModel().getProperties().putValue(PreviewProperty.EDGE_COLOR, new EdgeColor(EdgeColor.Mode.ORIGINAL));

        RankingElementColorTransformer degreeTransformer = function.getTransformer();
        degreeTransformer.setColors(new Color[]{color1, color2});
        degreeTransformer.setColorPositions(new float[]{0f, 1f});
        appearanceController.transform(function);

        refreshPreview();

        eColor         = null;
        eRankingColor1 = color1;
        eRankingColor2 = color2;
        intraColor     = null;
        interColor     = null;
    }

    private static Palette getPalette(String paletteName, int size) {
        for (Map.Entry<String, Palette> entry : palettes.entrySet()) {
            if (entry.getKey().equals(paletteName)) {
                return entry.getValue();
            }
        }
        for (Preset preset : PaletteManager.getInstance().getPresets()) {
            if (preset.getName().equals(paletteName)) {
                Palette palette = PaletteManager.getInstance().generatePalette(size, preset);
                palettes.put(paletteName, palette);
                return palette;
            }
        }
        return PaletteManager.getInstance().generatePalette(size);
    }

    private static void initializePalettes() {
        Column column = Project.getGraphModel().getNodeTable().getColumn(GephiInitializer.CLUSTER);
        Function func = appearanceModel.getNodeFunction(Project.getGraphModel().getDirectedGraph(), column, PartitionElementColorTransformer.class);
        partition = ((PartitionFunction) func).getPartition();

        Group.init();

        PaletteManager.getInstance().getPresets()
                .parallelStream()
                .forEach(preset -> {
                    Palette palette = PaletteManager.getInstance().generatePalette(partition.size(), preset);
                    palettes.put(preset.getName(), palette);
                });
    }

    public static void reset() {
        palettes  = new HashMap<>();
        partition = null;

        nodeMinSize  = Double.MAX_VALUE;
        nodeMaxSize  = Double.MIN_VALUE;
        labelMinSize = Double.MAX_VALUE;
        labelMaxSize = Double.MIN_VALUE;

        nSizeDegree     = false;
        nSizeInDegree   = false;
        nSizeOutDegree  = false;
        nColorDegree    = false;
        nColorInDegree  = false;
        nColorOutDegree = false;
        lSizeDegree     = false;
        lSizeInDegree   = false;
        lSizeOutDegree  = false;
        lColorDegree    = false;
        lColorInDegree  = false;
        lColorOutDegree = false;

        nColor         = null;
        nRankingColor1 = null;
        nRankingColor2 = null;
        lColor         = null;
        lRankingColor1 = null;
        lRankingColor2 = null;
        eColor         = null;
        eRankingColor1 = null;
        eRankingColor2 = null;
        intraColor     = null;
        interColor     = null;
    }

    @SuppressWarnings("Duplicates")
    public static void init() {
        appearanceController = Lookup.getDefault().lookup(AppearanceController.class);
        appearanceModel      = appearanceController.getModel();
        SwingWorker paletteWorker = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                Appearance.initializePalettes();
                return null;
            }
        };
        paletteWorker.execute();
        DirectedGraph directedGraph = Project.getGraphModel().getDirectedGraph();
        directedGraph.getNodes().forEach( node -> {
            nodeMinSize = node.size() < nodeMinSize ? node.size() : nodeMinSize;
            nodeMaxSize = node.size() > nodeMaxSize ? node.size() : nodeMaxSize;
        });

        if(nodeMinSize == nodeMaxSize){
            nSizeDegree    = false;
            nSizeInDegree  = false;
            nSizeOutDegree = false;
        } else {
            Node maxDegree    = Project.getGraphModel().factory().newNode();
            Node maxInDegree  = Project.getGraphModel().factory().newNode();
            Node maxOutDegree = Project.getGraphModel().factory().newNode();

            for (Node node : directedGraph.getNodes().toArray()) {
                maxDegree    = directedGraph.getDegree(node)    > directedGraph.getDegree(maxDegree)       ? node : maxDegree;
                maxInDegree  = directedGraph.getInDegree(node)  > directedGraph.getInDegree(maxInDegree)   ? node : maxInDegree;
                maxOutDegree = directedGraph.getOutDegree(node) > directedGraph.getOutDegree(maxOutDegree) ? node : maxOutDegree;
            }

            Node minDegree    = maxDegree;
            Node minInDegree  = maxInDegree;
            Node minOutDegree = maxOutDegree;

            for (Node node : directedGraph.getNodes().toArray()) {
                minDegree    = directedGraph.getDegree(node)    < directedGraph.getDegree(minDegree)       ? node : minDegree;
                minInDegree  = directedGraph.getInDegree(node)  < directedGraph.getInDegree(minInDegree)   ? node : minInDegree;
                minOutDegree = directedGraph.getOutDegree(node) < directedGraph.getOutDegree(minOutDegree) ? node : minOutDegree;
            }


            nSizeDegree    = minDegree.size()    <= minInDegree.size() && minDegree.size()    <= minOutDegree.size() && maxDegree.size()    >= maxInDegree.size() && maxDegree.size() >= maxOutDegree.size();
            nSizeInDegree  = minInDegree.size()  <= minDegree.size()   && minInDegree.size()  <= minOutDegree.size() && maxInDegree.size()  >= maxDegree.size()   && maxInDegree.size() >= maxOutDegree.size();
            nSizeOutDegree = minOutDegree.size() <= minDegree.size()   && minOutDegree.size() <= minInDegree.size()  && maxOutDegree.size() >= maxDegree.size()   && maxOutDegree.size() >= maxInDegree.size();

            if (nSizeDegree && nSizeInDegree) {
                for (Node n1 : directedGraph.getNodes()) {
                    for (Node n2 : directedGraph.getNodes()) {
                        if (directedGraph.getDegree(n1) == directedGraph.getDegree(n2)) {
                            if (n1.size() != n2.size()) {
                                nSizeDegree = false;
                                break;
                            } else if (directedGraph.getOutDegree(n1) != directedGraph.getOutDegree(n2)) {
                                nSizeDegree = false;
                                break;
                            }
                        }
                        if (directedGraph.getInDegree(n1) == directedGraph.getInDegree(n2)) {
                            if (n1.size() != n2.size()) {
                                nSizeInDegree = false;
                                break;
                            } else if (directedGraph.getOutDegree(n1) != directedGraph.getOutDegree(n2)) {
                                nSizeDegree = false;
                                break;
                            }
                        }
                    }
                }
            }
            if (nSizeDegree && nSizeOutDegree) {
                for (Node n1 : directedGraph.getNodes()) {
                    for (Node n2 : directedGraph.getNodes()) {
                        if (directedGraph.getDegree(n1) == directedGraph.getDegree(n2)) {
                            if (n1.size() != n2.size()) {
                                nSizeDegree = false;
                                break;
                            } else if (directedGraph.getInDegree(n1) != directedGraph.getInDegree(n2)) {
                                nSizeDegree = false;
                                break;
                            }
                        }
                        if (directedGraph.getOutDegree(n1) == directedGraph.getOutDegree(n2)) {
                            if (n1.size() != n2.size()) {
                                nSizeOutDegree = false;
                                break;
                            } else if (directedGraph.getInDegree(n1) != directedGraph.getInDegree(n2)) {
                                nSizeDegree = false;
                                break;
                            }
                        }
                    }
                }
            }
        }
        directedGraph.getNodes().forEach( node -> {
            labelMinSize = node.getTextProperties().getSize() < labelMinSize ? node.getTextProperties().getSize() : labelMinSize;
            labelMaxSize = node.getTextProperties().getSize() > labelMaxSize ? node.getTextProperties().getSize() : labelMaxSize;
        });
    }

}
