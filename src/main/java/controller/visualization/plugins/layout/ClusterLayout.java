package controller.visualization.plugins.layout;

import org.gephi.appearance.api.*;
import org.gephi.appearance.plugin.PartitionElementColorTransformer;
import org.gephi.filters.api.FilterController;
import org.gephi.filters.api.Query;
import org.gephi.filters.plugin.partition.PartitionBuilder;
import org.gephi.filters.spi.FilterBuilder;
import org.gephi.graph.api.Column;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphView;
import org.gephi.graph.api.Node;
import org.gephi.layout.plugin.AbstractLayout;
import org.gephi.layout.plugin.noverlap.NoverlapLayout;
import org.gephi.layout.plugin.noverlap.NoverlapLayoutBuilder;
import org.gephi.layout.spi.LayoutBuilder;
import org.gephi.layout.spi.LayoutProperty;
import org.gephi.project.api.Workspace;
import org.gephi.utils.longtask.spi.LongTask;
import org.gephi.utils.progress.ProgressTicket;
import org.openide.util.Lookup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Gewrgia
 */

@SuppressWarnings("unchecked")
public class ClusterLayout extends AbstractLayout implements org.gephi.layout.spi.Layout, LongTask {
    protected boolean cancel;
    protected Graph graph;
    private double margin;

    private FilterController filterController;
    private PartitionBuilder.NodePartitionFilter partitionFilter;
    private NoverlapLayout layout;

    private Workspace workspace;
    private Column column;
    private ArrayList<String> visiblePartitions;
    private ArrayList<String> partitions;

    public ClusterLayout(LayoutBuilder layoutBuilder) {
        super(layoutBuilder);
    }

    @Override
    public void initAlgo() {
        this.graph = this.graphModel.getGraphVisible();
        this.setConverged(false);
        this.cancel = false;
        this.filterController = Lookup.getDefault().lookup(FilterController.class);

        AppearanceController appearanceController = Lookup.getDefault().lookup(AppearanceController.class);
        AppearanceModel appearanceModel = appearanceController.getModel();

        PartitionBuilder pb = new PartitionBuilder();
        FilterBuilder[] fb = pb.getBuilders(this.workspace);

        if (fb.length < 1) {
            this.partitionFilter = new PartitionBuilder.NodePartitionFilter(this.column, appearanceModel);
        } else {
            for (FilterBuilder filterBuilder : fb) {
                try {
                     this.partitionFilter = (PartitionBuilder.NodePartitionFilter) filterBuilder.getFilter(this.workspace);
                    if(partitionFilter.getColumn() == this.column){
                        break;
                    }
                } catch (Exception ignored){}
            }
        }

        NoverlapLayoutBuilder builder = new NoverlapLayoutBuilder();
        this.layout = new NoverlapLayout(builder);
        this.layout.resetPropertiesValues();
        this.layout.setMargin(this.margin);
        this.layout.setGraphModel(this.graphModel);
        this.layout.setSpeed((double) 10);


        Function func = appearanceModel.getNodeFunction(this.graph, this.column, PartitionElementColorTransformer.class);
        Partition partition = ((PartitionFunction) func).getPartition();
        this.partitions = new ArrayList<>();
        ((Collection<String>) partition.getSortedValues())
                .stream()
                .filter(p -> {
                    for (Node n : this.graph.getNodes().toArray()) {
                        if (n.getAttribute(this.column.getId()).equals(p)) return true;
                    }
                    return false;
                })
                .forEach(this.partitions::add);
        this.visiblePartitions = new ArrayList<>(this.partitions);
    }

    @Override
    public void goAlgo() {
        if (this.partitions.size() <= 0) {
            setConverged(true);
            return;
        }
        this.graph = this.graphModel.getGraphVisible();
        this.graph.readLock();

        String partition = this.partitions.get(0);
        int maxDegree = Integer.MIN_VALUE;
        float x = 0;
        float y = 0;
        Set<Node> nodes = new HashSet<>();
        for (Node node : this.graph.getNodes().toCollection()) {
            if (node.getAttribute(this.column.getId()).equals(partition)) {
                nodes.add(node);
                if (this.graph.getDegree(node) > maxDegree) {
                    x = node.x();
                    y = node.y();
                    maxDegree = this.graph.getDegree(node);
                }
            }
        }

        this.partitionFilter.unselectAll();
        this.partitionFilter.addPart(partition);
        Query query = filterController.createQuery(this.partitionFilter);
        this.graph.readUnlock();
        GraphView view = this.filterController.filter(query);
        this.graphModel.setVisibleView(view);

        for (Node node : nodes) {
            node.setX(x);
            node.setY(y);
        }


        this.layout.initAlgo();
        while (this.layout.canAlgo()) {
            this.layout.goAlgo();
        }
        this.layout.endAlgo();


        this.partitionFilter.unselectAll();
        this.visiblePartitions.forEach(this.partitionFilter::addPart);
        query = this.filterController.createQuery(this.partitionFilter);
        view = this.filterController.filter(query);
        this.graphModel.setVisibleView(view);

        this.partitions.remove(0);
    }

    @Override
    public void endAlgo() {
        this.graph.getNodes().forEach(node -> node.setLayoutData(null));
    }

    @Override
    public LayoutProperty[] getProperties() {
        return new LayoutProperty[0];
    }

    @Override
    public void resetPropertiesValues() {
        this.margin = 5.0D;
    }

    public void setMargin(Double margin) {
        this.margin = margin;
    }

    public void setWorkspace(Workspace workspace) {
        this.workspace = workspace;
    }

    public void setColumn(Column column) {
        this.column = column;
    }

    @Override
    public boolean cancel() {
        this.cancel = true;
        return true;
    }

    @Override
    public void setProgressTicket(ProgressTicket progressTicket) {

    }
}
