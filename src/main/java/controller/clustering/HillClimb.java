package controller.clustering;

import model.api.Cluster;
import model.api.Graph;
import model.api.MDG;

import java.util.Arrays;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Georgia Grigoriadou
 */

public class HillClimb implements ClusteringAlgorithm{

    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/HillClimb");

    @Override
    public MDG cluster(Graph graph, MDG mdg, Cluster cluster, int iterations) {
        if(iterations == 0) {
            int nodeCount = graph.getNodes().size();
            iterations = nodeCount < 100 ?  Integer.parseInt(bundle.getString("HillClimb.under100"))
                    : nodeCount < 500 ?  Integer.parseInt(bundle.getString("HillClimb.under500"))
                    : nodeCount < 1000 ? Integer.parseInt(bundle.getString("HillClimb.under1000"))
                    : Integer.parseInt(bundle.getString("HillClimb.above1000"));
        }
        NeighbourPartitionGenerator npg = new NeighbourPartitionGenerator();
        RandomClusterGenerator rcg      = new RandomClusterGenerator();
        MDG[] initialPopulationStream   = new MDG[iterations];

        Arrays.parallelSetAll(initialPopulationStream, i -> rcg.generate.apply(graph, mdg, cluster));

        Set<MDG> initialPopulation         = new HashSet<>(Arrays.asList(initialPopulationStream));
        AtomicReference<MDG> bestPartition = new AtomicReference<>(initialPopulation.iterator().next());
        AtomicReference<Float> maxMQ       = new AtomicReference<>(bestPartition.get().getModularizationQuality());
        AtomicReference<Float> currentMQ   = new AtomicReference<>();

        initialPopulation.parallelStream().forEach((partition) -> {
            MDG currentPartition = partition;
            MDG nextPartition    = npg.generate.apply(currentPartition);

            while (nextPartition.getModularizationQuality() > currentPartition.getModularizationQuality()) {
                currentPartition = nextPartition;
                nextPartition    = npg.generate.apply(currentPartition);
            }
            currentMQ.set(currentPartition.getModularizationQuality());
            if (currentMQ.get() > maxMQ.get()) {
                bestPartition.set(currentPartition);
                maxMQ.set(currentMQ.get());
            }

        });

        bestPartition.get().setClusterIds();
        return bestPartition.get();
    }
}
