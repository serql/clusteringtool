package controller.clustering;

import model.BiUnaryOperator;
import model.TriFunction;
import model.api.Cluster;
import model.api.MDG;
import model.api.Node;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

/**
 * @author Georgia Grigoriadou
 */

public class NeighbourPartitionGenerator {

    public UnaryOperator<MDG> generate = (partition) -> {
        AtomicReference<MDG> currentPartition = new AtomicReference<>(partition);
        currentPartition.get().getClusters().forEach(cluster ->
                cluster.getNodes().forEach(node ->
                        currentPartition.set(createNeighbourPartition.apply(currentPartition.get(), cluster, node))));

        currentPartition.get().deleteEmptyClusters();
        return currentPartition.get();
    };

    private static TriFunction<MDG, Cluster, Node, MDG> createNeighbourPartition = (MDG initialPartition, Cluster initialCluster, Node node) -> {
        MDG bestPartition = initialPartition;

        MDG part1 = NeighbourPartitionGenerator.createNPByMovingInExistingCluster.apply(initialPartition, initialCluster, node);
        MDG part2 = NeighbourPartitionGenerator.createNPByAddingANewCluster.apply(initialPartition, initialCluster, node);
        MDG part3 = NeighbourPartitionGenerator.createNPByRemovingABlock.apply(initialPartition, initialCluster, node);

        if (part1.getModularizationQuality() > part2.getModularizationQuality() && part1.getModularizationQuality() > part3.getModularizationQuality()) {
            bestPartition = part1;
        } else if (part2.getModularizationQuality() > part3.getModularizationQuality()) {
            bestPartition = part2;
        } else {
            bestPartition = part3;
        }
        return bestPartition;
    };


    private static TriFunction<MDG, Cluster, Node, MDG> createNPByMovingInExistingCluster = (MDG initialPartition, Cluster initialCluster, Node node) -> {

        AtomicReference<MDG> bestPartition = new AtomicReference<>(initialPartition);
        Set<Cluster> alreadyAddedClusters = new HashSet<>();
        alreadyAddedClusters.add(initialCluster);

        node.getNeighbours().entrySet().parallelStream()
                .filter(neighbour -> !alreadyAddedClusters.contains(initialPartition.getClusterOfTargetNode(neighbour.getKey())))
                .forEach(neighbour -> {

                    MDG partition            = initialPartition.clone();
                    Cluster cluster          = partition.getClusterOfTargetNode(node);
                    Cluster neighbourCluster = partition.getClusterOfTargetNode(neighbour.getKey());

                    partition.removeNodeFromTargetCluster(cluster, node);
                    partition.addNodeToTargetCluster(neighbourCluster, node);
                    alreadyAddedClusters.add(initialPartition.getClusterOfTargetNode(neighbour.getKey()));

                    if(cluster.getNodes().size() == 1){
                        Object[] nodes = cluster.getNodes().toArray();
                           partition = NeighbourPartitionGenerator.compare.apply(partition, NeighbourPartitionGenerator.createNPByMovingInExistingCluster.apply(partition, cluster, (Node) nodes[0]));
                    }
                    bestPartition.set(NeighbourPartitionGenerator.compare.apply(bestPartition.get(), partition));
                });
        return bestPartition.get();
    };

    private static TriFunction<MDG, Cluster, Node, MDG> createNPByAddingANewCluster = (MDG initialPartition, Cluster initialCluster, Node node) -> {
        if (initialCluster.getNodes().size() == 1) { return initialPartition; }

        MDG partition            = initialPartition.clone();
        Cluster cluster          = partition.getClusterOfTargetNode(node);
        Cluster neighbourCluster = cluster.clone();

        partition.removeNodeFromTargetCluster(cluster, node);
        Set<Node> ns = neighbourCluster.getNodes().stream().filter(n -> n != node).collect(Collectors.toSet());
        ns.forEach(neighbourCluster::removeNode);
        partition.addCluster(neighbourCluster);
        if(cluster.getNodes().size() == 1){
            Object[] nodes = cluster.getNodes().toArray();
            partition = NeighbourPartitionGenerator.compare.apply(partition, NeighbourPartitionGenerator.createNeighbourPartition.apply(partition,cluster, (Node) nodes[0]));
        }
        partition = NeighbourPartitionGenerator.compare.apply(initialPartition, partition);

        return partition;
    };

    private static TriFunction<MDG, Cluster, Node, MDG> createNPByRemovingABlock = (MDG initialPartition, Cluster initialCluster, Node node) -> {

        AtomicReference<MDG> bestPartition = new AtomicReference<>(initialPartition);

        node.getNeighbours().entrySet().parallelStream()
             //   .filter (neighbour -> !(initialCluster.getNodes().size() == 2 && initialCluster.getNodes().contains(neighbour.getKey())))
                .forEach(neighbour -> {

                    MDG partition            = initialPartition.clone();
                    Cluster cluster          = partition.getClusterOfTargetNode(node);
                    Cluster neighbourCluster = partition.getClusterOfTargetNode(neighbour.getKey());
                    Cluster newCluster       = cluster.clone();

                    Set<Node> ns = newCluster.getNodes().stream().filter(n -> n != node).collect(Collectors.toSet());
                    ns.forEach(newCluster::removeNode);
                    partition.removeNodeFromTargetCluster(cluster, node);
                    partition.removeNodeFromTargetCluster(neighbourCluster, neighbour.getKey());
                    partition.addCluster(newCluster);
                    partition.addNodeToTargetCluster(newCluster, neighbour.getKey());
                    if(cluster.getNodes().size() == 1){
                        Object[] nodes = cluster.getNodes().toArray();
                        partition = NeighbourPartitionGenerator.compare.apply(partition, NeighbourPartitionGenerator.createNPByMovingInExistingCluster.apply(partition,cluster, (Node) nodes[0]));
                    }
                    bestPartition.set(NeighbourPartitionGenerator.compare.apply(bestPartition.get(), partition));
                });

        return bestPartition.get();
    };

    private static BiUnaryOperator<MDG> compare = (partition1, partition2)->{
        if (partition1.getModularizationQuality() < partition2.getModularizationQuality()){
            return partition2;
        }
        return partition1;
    };

}


