import gui.SheafFrame;

import javax.swing.*;

/**
 * @author Georgia Grigoriadou
 */

public class Application {

    public static void main(String[] args) {
            SwingUtilities.invokeLater(SheafFrame::new);
    }

}
